#version 330 core

/*
 * This is a simple instanced-geometry drawing shader.
 * However, it can also be used for non-instanced drawing,
 * In which case it's a good idea to set symbol_geometry to zero.
 *
 */

layout (location = 0) in vec2 symbol_geometry;
layout (location = 1) in vec2 point_location;

uniform mat4 projection;
uniform mat4 modelview;

out vec2 tex_coord;

uniform float symbol_scale;

void main(void) {

	vec4 position = modelview * vec4(point_location, 0.0, 1.0); // get the point_location by modelview first, so that symbol_geometry isn't also transformed
	
	position.xy += symbol_geometry * symbol_scale; // if symbol_geometry == zero this shouldn't do anything
	
	tex_coord = symbol_geometry;
	
	gl_Position = projection * position; // we can round position if we don't want symbols to get distorted...
}