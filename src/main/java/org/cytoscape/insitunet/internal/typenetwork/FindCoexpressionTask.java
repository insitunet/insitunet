package org.cytoscape.insitunet.internal.typenetwork;

import java.util.List;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.cytoscape.insitunet.internal.InsituNetActivator;
import org.cytoscape.insitunet.internal.gl.Point2D;
import org.cytoscape.insitunet.internal.gl.Shape2D;
import org.cytoscape.insitunet.internal.panel.ControlSet;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import edu.wlu.cs.levy.CG.KDTree;
import edu.wlu.cs.levy.CG.KeySizeException;

public class FindCoexpressionTask extends AbstractTask {

	final KDTree<Transcript> tree;
	final Array2DRowRealMatrix coexpressionMatrix;
	final FilteredNetwork network;
	final int[] counts;
	final List<Shape2D> shapes;
	final ControlSet controls;

	InsituNetActivator ia;

	public FindCoexpressionTask(FilteredNetwork network, ControlSet controls, List<Shape2D> shapes,
			KDTree<Transcript> tree, int size) {

		this.network = network;
		this.controls = controls;
		this.shapes = shapes;
		this.tree = tree;
		this.coexpressionMatrix = new Array2DRowRealMatrix(size, size); // Square matrix
		this.counts = new int[size];
	}

	/**
	 * Constructs a KDTree from the list given during initialization. Running this
	 * as a Task allows us to display progress.
	 */
	@Override
	public void run(final TaskMonitor monitor) {

		calculateCoexpression(monitor);
	}

	/**
	 * Fill the matrix with coexpression info
	 */
	public void calculateCoexpression(final TaskMonitor monitor) {

		for (Shape2D shape : shapes) {

			// Reset counts
			for (int i = 0; i < coexpressionMatrix.getRowDimension(); i++) {
				double[] empty = new double[coexpressionMatrix.getColumnDimension()];
				coexpressionMatrix.setRow(i, empty);
			}
			for (int i = 0; i < counts.length; i++) {
				counts[i] = 0;
			}

			if (network.searchDistance.compareTo(controls.searchDistancePixels) != 0
					|| !network.searchShape.equals(shape)) {

				monitor.setTitle("Finding coexpressed transcripts");
				network.searchDistance = controls.searchDistancePixels;
				monitor.setStatusMessage("(distance = " + network.searchDistance + ")");
				network.searchShape = new Shape2D(shape);

				// Get list of every transcript within the bounding rectangle of the selected
				// shape
				Shape2D bounds = network.searchShape.getBoundingRectangle();
				Point2D min = bounds.get(0);
				Point2D max = bounds.get(2);

				List<Transcript> range = tree.range(new double[] { min.x, min.y }, new double[] { max.x, max.y });

				// For each transcript, test if it is in the actual shape, then find neighboring
				// transcripts
				int i = 0;
				for (Transcript t : range) {

					if (cancelled)
						break;
					if (!network.searchShape.contains(new Point2D(t.x, t.y)))
						continue;
					monitor.setProgress((double) i++ / range.size());
					counts[t.getGene().getIndex()] += 1;

					double[] row = coexpressionMatrix.getRow(t.getGene().getIndex());

					List<Transcript> neighbours;
					try {
						neighbours = tree.nearestEuclidean(new double[] { t.getX(), t.getY() }, network.searchDistance);// need
																														// squared
																														// distance
						neighbours.remove(neighbours.size() - 1); // this is the selected transcript
					} catch (KeySizeException e) {
						e.printStackTrace();
						continue;
					}
					for (Transcript n : neighbours) {
						if (!network.searchShape.contains(new Point2D(n.x, n.y)))
							continue;
						row[n.getGene().getIndex()] += 1;
					}
					coexpressionMatrix.setRow(t.getGene().getIndex(), row);
				}
				int numberOfComparisons = 0;
				int numberOfCoexpressions = 0;
				for (i = 0; i < coexpressionMatrix.getRowDimension(); i++) {
					double[] row = coexpressionMatrix.getRow(i);
					for (int j = i; j < row.length; j++) { // add 1 to j here to avoid having the same transcript type
															// recorded
						if (row[j] > 0) {
							numberOfComparisons++;
							if (i == j) {
								numberOfCoexpressions += row[j] / 2; // because we double up in the center
							} else {
								numberOfCoexpressions += row[j];
							}
						}
					}
				}
				network.setComparisons(numberOfComparisons);
				network.setCoexpressions(numberOfCoexpressions);

				network.setMatrix(coexpressionMatrix);
				network.setCounts(counts);

			}

			monitor.setTitle("Filtering");
			monitor.setStatusMessage("Applying any selected filtering");

			switch (controls.testIndex) {
			case 0:
				network.filterWithShuffle(controls);
				break;
			case 1:
				network.filterWithHypergeometric(controls);
				break;
			case 2:
				network.filterWithNothing(controls);
				break;
			}

		}

	}
}
