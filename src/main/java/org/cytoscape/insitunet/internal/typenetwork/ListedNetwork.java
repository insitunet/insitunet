package org.cytoscape.insitunet.internal.typenetwork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.cytoscape.insitunet.internal.InsituDataset;
import org.cytoscape.insitunet.internal.InsituNetActivator;
import org.cytoscape.insitunet.internal.gl.Shape2D;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.session.CySession;
import org.cytoscape.view.model.CyNetworkView;

/**
 * The filtered network view and shape.
 * 
 * @author John Salamon
 */
public class ListedNetwork implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6897277517696420315L;
	transient CyNetwork network;
	transient List<CyNode> nodes;
	transient CyNetworkView view;
	// Can't serialize the actual CyWhatevers
	// So just serialize their SUIDs
	Long netSUID; 
	List<Long> nodesSUID;
	Long viewSUID;

	Shape2D shape;
	final Long timestamp;
	transient InsituDataset dataset;

	public ListedNetwork(CyNetwork network, InsituDataset dataset, List<CyNode> nodes, CyNetworkView view,
			Long timestamp) {
		this.dataset = dataset;
		this.network = network;
		this.nodes = nodes;
		this.view = view;
		this.timestamp = timestamp;
		
		makeSUIDsWhatTheyActuallyAreCurrentlyBecauseTheyDontStayTheSameWhenYouReloadASessionFromFileForSomeUnkownReason();
	}
	
	void makeSUIDsWhatTheyActuallyAreCurrentlyBecauseTheyDontStayTheSameWhenYouReloadASessionFromFileForSomeUnkownReason() {
		this.netSUID = network.getSUID();
		this.nodesSUID = new ArrayList<>();
		for (CyNode node : nodes) {
			this.nodesSUID.add(node.getSUID());
		}
		this.viewSUID = view.getSUID();
	}

	public CyNetworkView getView() {
		return view;
	}

	public List<CyNode> getNodes() {
		return nodes;
	}

	public CyNetwork getNetwork() {
		return network;
	}

	public Shape2D getShape() {
		return shape;
	}

	public void setShape(Shape2D shape) {
		this.shape = shape;
	}

	@Override
	public String toString() {
		return "V: " + network.getNodeCount() + " E: " + network.getEdgeCount() + " [" + timestamp + "]";
	}

	public InsituDataset getDataset() {
		return dataset;
	}

	public void rehydrate(CySession session, InsituDataset dataset) {
		this.dataset = dataset;
		network = session.getObject(netSUID, CyNetwork.class);
		view = session.getObject(viewSUID, CyNetworkView.class);
		ArrayList<CyNode> nodesTemp = new ArrayList<>();
		for (Long id : nodesSUID) {
			nodesTemp.add(session.getObject(id, CyNode.class));
		}
		nodes = nodesTemp;
		makeSUIDsWhatTheyActuallyAreCurrentlyBecauseTheyDontStayTheSameWhenYouReloadASessionFromFileForSomeUnkownReason();
	}
}
