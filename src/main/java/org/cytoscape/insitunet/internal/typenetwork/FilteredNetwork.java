package org.cytoscape.insitunet.internal.typenetwork;

import java.awt.Color;
import java.awt.Paint;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.distribution.HypergeometricDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.insitunet.internal.Gene;
import org.cytoscape.insitunet.internal.InsituDataset;
import org.cytoscape.insitunet.internal.gl.Shape2D;
import org.cytoscape.insitunet.internal.panel.ControlSet;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.vizmap.VisualMappingFunction;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.view.vizmap.mappings.DiscreteMapping;
import org.cytoscape.work.SynchronousTaskManager;
import org.cytoscape.work.TaskIterator;

import edu.wlu.cs.levy.CG.KDTree;

/**
 * The base UnfilteredNetwork for a dataset will contain every possible edge, as
 * all coexpression events are included.
 * 
 * @author jrs
 *
 */
public class FilteredNetwork implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8668492420443844426L;

	final InsituDataset dataset;

	Double maxZ = 0d;

	Array2DRowRealMatrix coexpressionMatrix;
	
	boolean uniqueMode = false;

	Double searchDistance = Double.NaN;
	Shape2D searchShape = new Shape2D();

	int[] counts; // contains how many of each transcript type there is
	int transcriptTotal; // contains the total number of transcripts
	int coexpressionTotal; // contains how many 1to1 coexpression events there are

	transient VisualStyle style;

	transient VisualMappingFunction<Boolean, Paint> edgeUniqueHighlight;
	transient VisualMappingFunction<String, Paint> edgeColour;

	private int comparisons = 1; // how many comparisons are we doing (for multiple testing correction)

	/**
	 * 
	 * @param size
	 * @throws InterruptedException
	 */
	public FilteredNetwork(InsituDataset dataset) throws InterruptedException {
		this.dataset = dataset;
		CyAppAdapter adapter = dataset.getActivator().getCAA();

		coexpressionMatrix = new Array2DRowRealMatrix(dataset.getGenes().size(), dataset.getGenes().size());
		this.style = ViewStyler.generateStyle(dataset, adapter);
		this.edgeColour = ViewStyler.getEdgeColourFunction(adapter);
		this.edgeUniqueHighlight = ViewStyler.getEdgeUniqueFunction(adapter, Color.RED);
	}

	public void doSearch(KDTree<Transcript> tree, CySwingAppAdapter adapter, ControlSet controls, List<Shape2D> windows)
			throws InterruptedException {
		TaskIterator iterator = new TaskIterator();

		FindCoexpressionTask fcTask = new FindCoexpressionTask(this, controls, windows, tree,
				dataset.getGenes().size());
		iterator.append(fcTask);
		adapter.getDialogTaskManager().execute(iterator);
	}

	public void setMatrix(Array2DRowRealMatrix matrix) {
		this.coexpressionMatrix = matrix;
	}

	public void setCounts(int[] counts) {
		this.counts = counts;
		int total = 0;
		for (int i = 0; i < counts.length; i++) {
			total += counts[i];
		}
		this.transcriptTotal = total;
	}
	
	public void rehydrate(CyAppAdapter adapter, List<ListedNetwork> list) {
		this.style = ViewStyler.generateStyle(dataset, adapter);
		this.edgeColour = ViewStyler.getEdgeColourFunction(adapter);
		this.edgeUniqueHighlight = ViewStyler.getEdgeUniqueFunction(adapter, Color.RED);

		for(ListedNetwork ln : list) {
			updateStyle(ln.view);
		}

		for (Gene gene : dataset.getGenes()) {
			updateColour(gene);
		}
	}

	public void setHighlightColour(Color colour) {
		CyAppAdapter adapter = dataset.getActivator().getCAA();
		this.edgeUniqueHighlight = ViewStyler.getEdgeUniqueFunction(adapter, colour);
		if(uniqueMode) {
			style.addVisualMappingFunction(edgeUniqueHighlight);
			for (int i = 0; i < dataset.getControls().model.getSize(); i++) {
				ListedNetwork ln = (ListedNetwork) dataset.getControls().model.getElementAt(i);
				style.apply(ln.view);
			}
		}
	}
	
	public void setHighlight(boolean highlight) {

		if (highlight) {
			uniqueMode = true;
			style.addVisualMappingFunction(edgeUniqueHighlight);
		} else {
			uniqueMode = false;
			style.addVisualMappingFunction(edgeColour);
		}
		for (int i = 0; i < dataset.getControls().model.getSize(); i++) {
			ListedNetwork ln = (ListedNetwork) dataset.getControls().model.getElementAt(i);
			style.apply(ln.view);
		}

	}

	public void updateColour(Gene gene) {

		@SuppressWarnings("unchecked")
		VisualMappingFunction<String, Paint> vfb = (VisualMappingFunction<String, Paint>) style
				.getVisualMappingFunction(BasicVisualLexicon.NODE_BORDER_PAINT);
		((DiscreteMapping<String, Paint>) vfb).putMapValue(gene.getName(), gene.getColor());

		@SuppressWarnings("unchecked")
		VisualMappingFunction<String, Paint> vff = (VisualMappingFunction<String, Paint>) style
				.getVisualMappingFunction(BasicVisualLexicon.NODE_FILL_COLOR);
		((DiscreteMapping<String, Paint>) vff).putMapValue(gene.getName(), gene.getColor());

		for (int i = 0; i < dataset.getControls().model.getSize(); i++) {
			ListedNetwork ln = (ListedNetwork) dataset.getControls().model.getElementAt(i);
			style.apply(ln.view);
		}

	}

	public void updateZ(double maxZ) {

		@SuppressWarnings("unchecked")
		VisualMappingFunction<Double, Double> edgeWidth = (VisualMappingFunction<Double, Double>) style
				.getVisualMappingFunction(BasicVisualLexicon.EDGE_WIDTH);
		((ContinuousMapping<Double, Double>) edgeWidth).removePoint(0);
		((ContinuousMapping<Double, Double>) edgeWidth).removePoint(0);

		((ContinuousMapping<Double, Double>) edgeWidth).addPoint(1d, new BoundaryRangeValues<Double>(1d, 1d, 1d));
		((ContinuousMapping<Double, Double>) edgeWidth).addPoint(maxZ, new BoundaryRangeValues<Double>(15d, 15d, 15d));

		style.addVisualMappingFunction(edgeWidth);

		for (int i = 0; i < dataset.getControls().model.getSize(); i++) {
			ListedNetwork ln = (ListedNetwork) dataset.getControls().model.getElementAt(i);
			style.apply(ln.view);
		}

	}

	public void updateStyle(CyNetworkView view) {
		/*
		 * VisualMappingFunction<String, Double> vf = (VisualMappingFunction<String,
		 * Double>) style.getVisualMappingFunction(BasicVisualLexicon.NODE_SIZE);
		 * for(int i = 0; i < dataset.getGenes().size(); i++) { Gene gene =
		 * dataset.getGenes().get(i); double proportion =
		 * (double)counts[gene.getIndex()] / (double)transcriptTotal; double size =
		 * proportion * 200d + 10; // puts in range of 10 to 100
		 * ((DiscreteMapping<String, Double>)vf).putMapValue(gene.getName(), size); }
		 * 
		 * VisualMappingFunction<Double,Double> edgeWidth =
		 * (VisualMappingFunction<Double, Double>)
		 * style.getVisualMappingFunction(BasicVisualLexicon.EDGE_WIDTH);
		 * ((ContinuousMapping<Double,Double>)edgeWidth).removePoint(0);
		 * ((ContinuousMapping<Double,Double>)edgeWidth).removePoint(0);
		 * 
		 * //double val = maxZ > 10d ? maxZ : 10d;
		 * ((ContinuousMapping<Double,Double>)edgeWidth).addPoint(1d,new
		 * BoundaryRangeValues<Double>(1d,1d,1d));
		 * ((ContinuousMapping<Double,Double>)edgeWidth).addPoint(10d,new
		 * BoundaryRangeValues<Double>(10d,10d,10d));
		 * 
		 * style.addVisualMappingFunction(edgeWidth);
		 */
		style.apply(view);

		CyAppAdapter adapter = dataset.getActivator().getCAA();
		adapter.getVisualMappingManager().setVisualStyle(style, view);
		// adapter.getVisualMappingManager().setCurrentVisualStyle(style);

	}

	/*
	 * Premultiplies the coexpression matrix by a vector of 1.0's to extract total
	 * coexpression events for each type
	 */
	public double[] individualScores() {
		double[] vector = new double[coexpressionMatrix.getColumnDimension()];
		Arrays.fill(vector, 1.0);
		return coexpressionMatrix.preMultiply(vector);
	}

	public void printout() {
		for (int i = 0; i < coexpressionMatrix.getRowDimension(); i++) {
			double[] row = coexpressionMatrix.getRow(i);
			String line = "";
			for (int j = 0; j < row.length; j++) {
				line += row[j] + ", ";
			}
			System.out.println(line);
		}
	}

	/**
	 * Creates a CyNetwork with all the nodes in this dataset, no edges currently
	 */
	private CyNetwork makeBlankNetwork(CyAppAdapter adapter, List<CyNode> nodes) {
		CyNetwork network = adapter.getCyNetworkFactory().createNetwork();
		CyTable nodeTable = network.getDefaultNodeTable();
		nodeTable.createColumn("count", Integer.class, false); // The raw count of this transcript
		nodeTable.createColumn("proportion", Double.class, false); // The relative proportion

		CyTable edgeTable = network.getDefaultEdgeTable();
		edgeTable.createColumn("count", Integer.class, false); // The number of coexpression events of these two
																// transcripts
		edgeTable.createColumn("mean", Integer.class, false); // Distribution mean
		edgeTable.createColumn("z_score", Double.class, false); // Z score
		edgeTable.createColumn("pvalue", Double.class, false); // Caclulated pvalue, if any
		edgeTable.createColumn("unique", Boolean.class, false); // Whether this edge is unique within the currently
																// synced networks

		network.getRow(network).set(CyNetwork.NAME, dataset.toString());
		for (Gene g : dataset.getGenes()) {
			CyNode node = network.addNode();
			CyRow row = nodeTable.getRow(node.getSUID());
			nodes.add(node);
			row.set(CyNetwork.NAME, g.getName());
			row.set("count", g.getTranscripts().size());
			row.set("proportion", (double) g.getTranscripts().size() / (double) dataset.getTotalSize());
		}

		return network;
	}

	public void filterWithNothing(ControlSet controls) {
		ListedNetwork ln;
		if (controls.overwrite && !controls.model.isEmpty() && controls.selected > -1) {
			ln = (ListedNetwork) controls.model.getElementAt(controls.selected);
			ln.getNetwork().removeEdges(ln.getNetwork().getEdgeList()); // remove all edges
		} else {
			ln = newNet();
		}
		ln.setShape(searchShape);

		for (int i = 0; i < coexpressionMatrix.getRowDimension(); i++) {
			double[] row = coexpressionMatrix.getRow(i);
			for (int j = i + 1; j < row.length; j++) {
				if (row[j] > 0) {
					CyEdge edge = ln.getNetwork().addEdge(ln.nodes.get(i), ln.nodes.get(j), false);
					CyRow cyRow = ln.getNetwork().getDefaultEdgeTable().getRow(edge.getSUID());
					cyRow.set(CyNetwork.NAME, dataset.getGenes().get(i) + "-" + dataset.getGenes().get(j));
					cyRow.set("count", (int) row[j]);
				}
			}
		}
		updateStyle(ln.view);

		CyAppAdapter adapter = dataset.getActivator().getCAA();
		CyLayoutAlgorithm algo = controls.algorithm;
		SynchronousTaskManager<?> synTaskMan = adapter.getCyServiceRegistrar().getService(SynchronousTaskManager.class);
		TaskIterator iterator = algo.createTaskIterator(ln.view, algo.createLayoutContext(),
				CyLayoutAlgorithm.ALL_NODE_VIEWS, null);
		synTaskMan.execute(iterator);

		if (!controls.model.contains(ln)) {
			controls.model.addElement(ln);
		}

		CyTable nodeTable = ln.network.getDefaultNodeTable();
		for (int i = 0; i < dataset.getGenes().size(); i++) {
			Gene gene = dataset.getGenes().get(i);
			CyRow row = nodeTable.getRow(ln.nodes.get(i).getSUID());
			double proportion = (double) counts[gene.getIndex()] / (double) transcriptTotal;
			row.set("proportion", proportion);
		}
	}

	public void filterWithHypergeometric(ControlSet controls) {
		ListedNetwork listedNetwork;
		if (controls.overwrite && !controls.model.isEmpty() && controls.selected > -1) {
			listedNetwork = (ListedNetwork) controls.model.getElementAt(controls.selected);
			listedNetwork.getNetwork().removeEdges(listedNetwork.getNetwork().getEdgeList()); // remove all edges
		} else {
			listedNetwork = newNet();
		}
		listedNetwork.setShape(searchShape);
		// need an option to either do this, or remake an old one
		// System.out.println("network has " + newNet.getEdgeCount() + " edges and " +
		// newNet.getNodeCount() + " nodes");
		// baseNetwork.removeEdges(baseNetwork.getEdgeList()); // not needed bcos we
		// don't add to basenetwork ( I think)
		maxZ = 0d;

		for (int i = 0; i < coexpressionMatrix.getRowDimension(); i++) {
			double[] row = coexpressionMatrix.getRow(i);
			for (int j = i + 1; j < row.length; j++) {
				if (row[j] <= 0)
					continue;

				HypergeometricDistribution d = new HypergeometricDistribution(transcriptTotal, counts[i], counts[j]);
				double distributionMean = d.getNumericalMean();

				int coExpCount = (int) row[j] / 2;

				decideOnEdge(listedNetwork, i, j, distributionMean, d.probability(coExpCount),
						d.cumulativeProbability(coExpCount), controls, coExpCount);
			}
		}
		updateStyle(listedNetwork.view);

		CyAppAdapter adapter = dataset.getActivator().getCAA();
		CyLayoutAlgorithm algo = controls.algorithm;
		SynchronousTaskManager<?> synTaskMan = adapter.getCyServiceRegistrar().getService(SynchronousTaskManager.class);
		TaskIterator iterator = algo.createTaskIterator(listedNetwork.view, algo.createLayoutContext(),
				CyLayoutAlgorithm.ALL_NODE_VIEWS, null);
		synTaskMan.execute(iterator);

		if (!controls.model.contains(listedNetwork)) {
			controls.model.addElement(listedNetwork);
		}

		CyTable nodeTable = listedNetwork.network.getDefaultNodeTable();
		for (int i = 0; i < dataset.getGenes().size(); i++) {
			Gene gene = dataset.getGenes().get(i);
			CyRow row = nodeTable.getRow(listedNetwork.nodes.get(i).getSUID());
			double proportion = (double) counts[gene.getIndex()] / (double) transcriptTotal;
			row.set("proportion", proportion);
		}
	}

	public ListedNetwork newNet() {

		CyAppAdapter adapter = dataset.getActivator().getCAA();

		List<CyNode> nodes = new ArrayList<>();
		CyNetwork network = makeBlankNetwork(adapter, nodes);
		Long timestamp = System.currentTimeMillis();
		//network.getRow(network).set(CyNetwork.NAME, dataset.toString() + "_" + timestamp);

		adapter.getCyNetworkManager().addNetwork(network);

		CyNetworkView view = adapter.getCyNetworkViewFactory().createNetworkView(network);
		adapter.getCyNetworkViewManager().addNetworkView(view);
		adapter.getCyEventHelper().flushPayloadEvents();

		return new ListedNetwork(network, dataset, nodes, view, timestamp);
	}

	public void filterWithShuffle(ControlSet controls) {

		ListedNetwork listedNetwork;
		if (controls.overwrite && !controls.model.isEmpty() && controls.selected > -1) {
			listedNetwork = (ListedNetwork) controls.model.getElementAt(controls.selected);
			listedNetwork.getNetwork().removeEdges(listedNetwork.getNetwork().getEdgeList()); // remove all edges
		} else {
			listedNetwork = newNet();
		}
		listedNetwork.setShape(searchShape);

		maxZ = 0d;

		for (int i = 0; i < coexpressionMatrix.getRowDimension(); i++) {
			double[] row = coexpressionMatrix.getRow(i);
			for (int j = i + 1; j < row.length; j++) {
				if (row[j] <= 0)
					continue;

				int i_count, j_count, total;
				if (controls.useBackground) {
					i_count = dataset.getGenes().get(i).getTranscripts().size();
					j_count = dataset.getGenes().get(j).getTranscripts().size();
					total = dataset.getTotalSize();
				} else {
					i_count = counts[i];
					j_count = counts[j];
					total = transcriptTotal;
				}

				double distributionMean;
				int coExpCount = (int) row[j];
				if (i == j) { // Same transcript
					distributionMean = ((double) coexpressionTotal * (i_count * (double) i_count - i_count))
							/ ((double) total * total - total);
				} else {
					distributionMean = ((double) coexpressionTotal * 2 * i_count * j_count)
							/ ((double) total * total - total);
				}
				NormalDistribution d = new NormalDistribution(distributionMean, Math.sqrt(distributionMean));

				decideOnEdge(listedNetwork, i, j, distributionMean, d.probability(coExpCount),
						d.cumulativeProbability(coExpCount), controls, coExpCount);
			}
		}
		updateStyle(listedNetwork.view);

		CyAppAdapter adapter = dataset.getActivator().getCAA();
		CyLayoutAlgorithm algo = controls.algorithm;
		SynchronousTaskManager<?> synTaskMan = adapter.getCyServiceRegistrar().getService(SynchronousTaskManager.class);
		TaskIterator iterator = algo.createTaskIterator(listedNetwork.view, algo.createLayoutContext(),
				CyLayoutAlgorithm.ALL_NODE_VIEWS, null);
		synTaskMan.execute(iterator);

		if (!controls.model.contains(listedNetwork)) {
			controls.model.addElement(listedNetwork);
		}

		CyTable nodeTable = listedNetwork.network.getDefaultNodeTable();
		for (int i = 0; i < dataset.getGenes().size(); i++) {
			Gene gene = dataset.getGenes().get(i);
			CyRow row = nodeTable.getRow(listedNetwork.nodes.get(i).getSUID());
			double proportion = (double) counts[gene.getIndex()] / (double) transcriptTotal;
			row.set("proportion", proportion);
		}

	}

	/**
	 * Given the co-expression matrix location (in i and j) as well as the
	 * NormalDistribution, decide on whether to add an edge
	 */
	private void decideOnEdge(ListedNetwork ln, int i, int j, double m, double p, double pCu, ControlSet controls,
			int coExpCount) {

		double correction = controls.doCorrection ? comparisons : 1;
		double pvalue = -1;
		int interaction = -1;
		CyEdge edge = null;
		switch (controls.interactionIndex) {
		case 0:
			// Looking for more than expected, need to search the upper side
			pvalue = 1d - (pCu - p); // invert probability

			if (coExpCount < m || pvalue >= controls.pvalue / correction)
				break;
			edge = ln.network.addEdge(ln.nodes.get(i), ln.nodes.get(j), false);
			interaction = 1;
			break;
		case 1:
			// Looking for less than expected, need to search the lower side
			pvalue = pCu; // no need to invert

			if (coExpCount > m || pvalue >= controls.pvalue / correction)
				break;
			edge = ln.network.addEdge(ln.nodes.get(i), ln.nodes.get(j), false);
			interaction = 0;
			break;
		case 2:
			// Look both ways
			if (1d - (pCu - p) < controls.pvalue / correction) {
				pvalue = 1d - (pCu - p);
				edge = ln.network.addEdge(ln.nodes.get(i), ln.nodes.get(j), false);
				interaction = 1;
			} else if (pCu < controls.pvalue / correction) {
				pvalue = pCu;
				edge = ln.network.addEdge(ln.nodes.get(i), ln.nodes.get(j), false);
				interaction = 0;
			}
			break;
		}
		if (edge != null) {
			CyTable edgeTable = ln.network.getDefaultEdgeTable();
			CyRow cyRow = edgeTable.getRow(edge.getSUID());

			double zscore = ((double) coExpCount - m) / Math.sqrt(m);
			if (zscore > maxZ)
				maxZ = zscore;
			cyRow.set(CyNetwork.NAME, dataset.getGenes().get(i) + "-" + dataset.getGenes().get(j));
			cyRow.set("count", (int) coexpressionMatrix.getRow(i)[j]);
			cyRow.set("pvalue", pvalue);
			cyRow.set("z_score", Math.abs(zscore));
			cyRow.set("interaction", interaction > 0 ? "more" : "less");
		}
	}

	public void setComparisons(int numberOfComparisons) {
		this.comparisons = numberOfComparisons;
	}

	public void setCoexpressions(int numberOfCoexpressions) {
		this.coexpressionTotal = numberOfCoexpressions;
	}

	public VisualStyle getStyle() {
		return style;
	}
}
