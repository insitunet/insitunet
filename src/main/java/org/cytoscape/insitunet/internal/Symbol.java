package org.cytoscape.insitunet.internal;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.cytoscape.insitunet.internal.gl.Point2D;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.util.GLBuffers;

public class Symbol implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6189345028091250321L;
	final public int drawEnum;
	final float[] vertices;

	int offset; // to be set by the SymbolList

	/**
	 * Contains vertices and options for how to draw this symbol. All symbols are
	 * assumed to be within OpenGL NDC (ie. -1 to +1)
	 */
	public Symbol(float[] vertices, int drawEnum) {
		this.drawEnum = drawEnum;
		this.vertices = vertices;
	}

	public int getEnum() {
		return drawEnum;
	}

	public int getOffset() {
		return offset;
	}

	public int getLength() {
		return vertices.length;
	}

	public static class SymbolList {

		final static float[] square = new float[] { -1, -1, 1, -1, 1, 1, -1, 1, };

		final static float[] diagCross = new float[] { -1, -1, 1, 1, 1, -1, -1, 1, };

		final static float[] cross = new float[] { 0, -1, 0, 1, 1, 0, -1, 0, };

		final static float[] phatCross = new float[] { -0.3f, 0.3f, -0.3f, 1f, 0.3f, 1f, 0.3f, 0.3f, 1f, 0.3f, 1f,
				-0.3f, 0.3f, -0.3f, 0.3f, -1f, -0.3f, -1f, -0.3f, -0.3f, -1f, -0.3f, -1f, 0.3f, };

		final static float[] upTriangle = new float[] { -1, 1, 0, -1, 1, 1, };

		final static float[] downTriangle = new float[] { -1, -1, 0, 1, 1, -1, };

		public static List<Symbol> genSymbolList() {
			ArrayList<Symbol> list = new ArrayList<>();
			list.add(new Symbol(square, GL3.GL_LINE_LOOP));
			list.add(new Symbol(downTriangle, GL3.GL_LINE_LOOP));
			list.add(new Symbol(diagCross, GL3.GL_LINES));
			list.add(new Symbol(upTriangle, GL3.GL_LINE_LOOP));
			list.add(new Symbol(cross, GL3.GL_LINES));
			return list;
		}

		public static Integer makeBuffer(GL3 gl, List<Symbol> list) {

			// get total length and also offsets
			int length = 0;
			for (Symbol s : list) {
				s.offset = length;
				length += s.vertices.length;
			}

			// make the final FloatBuffer
			FloatBuffer buffer = FloatBuffer.allocate(length);
			int i = 0;
			for (Symbol s : list) {
				for (float f : s.vertices) {
					buffer.put(i++, f);
				}
			}
			int vbo[] = new int[1];
			gl.glGenBuffers(1, vbo, 0);
			int symbolVBO = vbo[0];
			int elementSize = GLBuffers.SIZEOF_FLOAT;
			int usage = GL3.GL_STATIC_DRAW;

			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, symbolVBO);
			gl.glBufferData(GL3.GL_ARRAY_BUFFER, buffer.capacity() * elementSize, buffer, usage);
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
			return symbolVBO;
		}
	}

	/**
	 * Generate a bufferedImage of this symbol at the desired dimensions. This is
	 * only used for the StylePanel, not OpenGL rendering.
	 */
	public BufferedImage makeImage(Dimension dim) {

		BufferedImage image = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_RGB);

		Graphics2D g2d = image.createGraphics();
		g2d.setBackground(Color.BLACK);
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, dim.width, dim.height);
		g2d.setColor(Color.WHITE);
		BasicStroke bs = new BasicStroke(2);
		g2d.setStroke(bs);

		switch (getEnum()) {
		case GL3.GL_LINE_LOOP:
			drawLoop(g2d, dim);
			break;
		case GL3.GL_LINES:
			drawLines(g2d, dim);
			break;
		}
		return image;
	}

	final float DIV = 2f;

	private void line(Graphics2D g2d, Point2D first, Point2D second, Dimension dim) {
		g2d.drawLine((int) Math.round((first.x / DIV + 1) / 2 * dim.width),
				(int) Math.round((first.y / DIV + 1) / 2 * dim.height),
				(int) Math.round((second.x / DIV + 1) / 2 * dim.width),
				(int) Math.round((second.y / DIV + 1) / 2 * dim.height));
	}

	/**
	 * Emulate the drawing style of GL_LINES
	 */
	private void drawLines(Graphics2D g2d, Dimension dim) {
		if (vertices.length % 4 != 0) {
			System.err.println("Wrong length array for GL_LINES");
			return; // can't be bothered throwing exceptions
		}
		for (int i = 0; i < vertices.length; i += 4) {
			Point2D first = new Point2D(vertices[i], vertices[i + 1]);
			Point2D second = new Point2D(vertices[i + 2], vertices[i + 3]);
			line(g2d, first, second, dim);
		}
	}

	/**
	 * Emulate the drawing style of GL_LINE_LOOP
	 */
	private void drawLoop(Graphics2D g2d, Dimension dim) {
		for (int i = 0; i < vertices.length; i += 2) {
			Point2D first, second;
			first = new Point2D(vertices[i], vertices[i + 1]);
			if (i == vertices.length - 2) {
				second = new Point2D(vertices[0], vertices[1]);
			} else {
				second = new Point2D(vertices[i + 2], vertices[i + 3]);
			}
			line(g2d, first, second, dim);
		}

	}
}
