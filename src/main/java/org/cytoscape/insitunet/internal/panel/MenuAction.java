package org.cytoscape.insitunet.internal.panel;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import org.cytoscape.util.swing.DropDownMenuButton;

public class MenuAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4388374128602999052L;
	JPopupMenu popup = new JPopupMenu();
	JMenuItem about = new JMenuItem("About");
	JMenuItem delete = new JMenuItem("Delete");

	private MainPanel main;

	public MenuAction(String text, MainPanel main) {
		super(text);
		this.main = main;
		delete.addActionListener(this);
		about.addActionListener(this);
		popup.add(about);
		popup.add(delete);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == delete) {
			main.deleteCurrent();
		} else if (e.getSource() == about) {
			String dataset = main.getCurrent().toString();
			String message = "Name: " + dataset + "\n" + "Total size: " + main.getCurrent().getTotalSize() + " points\n"
					+ "Unique types: " + main.getCurrent().getGenes().size();
			JOptionPane.showMessageDialog(null, message, "Dataset info", JOptionPane.INFORMATION_MESSAGE);
		} else {
			DropDownMenuButton b = (DropDownMenuButton) e.getSource();
			popup.show(b, 0, b.getHeight());
		}

	}

}
