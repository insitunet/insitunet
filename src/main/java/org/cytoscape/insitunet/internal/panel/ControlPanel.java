package org.cytoscape.insitunet.internal.panel;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.insitunet.internal.typenetwork.ListedNetwork;
import org.cytoscape.insitunet.internal.util.LayoutHelper;

/*
 * Allows the controls to be automatically set
 */
interface StateManager {
	void setControls(ControlSet set);

	ControlSet saveControls();

	ControlSet newFromCurrent(double x, double y);
}



public class ControlPanel extends JPanel {

	static final long serialVersionUID = 45353l;

	final LayoutHelper helper = new LayoutHelper();

	// Container for control state
	final StateManager stateManager;

	public ControlPanel(CyAppAdapter adapter, MainPanel main) {

		this.setLayout(new GridBagLayout());
		
		Point2D.Double pxSize = new Point2D.Double(10, 10);

		/****************
		 * NETWORK LIST *
		 ****************/
		JList<ListedNetwork> list = new JList<>(new DefaultListModel<>());
		list.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {

				if (list.getModel().getSize() > 0) {
					ListedNetwork ln = (ListedNetwork) list.getSelectedValue();
					if (ln == null || ln.getView() == null)
						return;
					adapter.getCyApplicationManager().setCurrentNetworkView(ln.getView());
					main.getCurrent().getPanel().getGLPanel().setShapeAs(ln.getShape());
					adapter.getVisualMappingManager().setVisualStyle(ln.getDataset().getStyle(), ln.getView());
				}
			}

		});

		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setMinimumSize(list.getPreferredScrollableViewportSize());

		JCheckBox makeNew = new JCheckBox("Overwrite selected network");
		makeNew.setSelected(false);

		JButton syncLayout = new JButton("Add selected to sync-list");
		syncLayout.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				main.addToSyncList(list.getSelectedValuesList());
			}

		});

		add(helper.makePanel("Network list", listScroller, makeNew, syncLayout), helper.makeConstraints());

		/********************
		 * DISTANCE CONTROL *
		 ********************/
		JSpinner xResSpinner = new JSpinner(new SpinnerNumberModel(100d, 0d, 10000d, 0.1d));
		JSpinner yResSpinner = new JSpinner(new SpinnerNumberModel(100d, 0d, 10000d, 0.1d));
		JSpinner distancePixelSpinner = new JSpinner(new SpinnerNumberModel(10d, 0d, 1000d, 0.1d));

		xResSpinner.setMaximumSize(new Dimension(80, xResSpinner.getPreferredSize().height));

		JPanel distancePanel = new JPanel();
		distancePanel.setLayout(new BoxLayout(distancePanel, BoxLayout.LINE_AXIS));
		JLabel dlabel = new JLabel("Search distance: ");
		distancePanel.add(dlabel);
		distancePixelSpinner.setMaximumSize(new Dimension(80, distancePixelSpinner.getPreferredSize().height));
		distancePanel.add(distancePixelSpinner);
		JLabel pxLabel = new JLabel("px");
		distancePanel.add(pxLabel);
		
		JLabel umResult = new JLabel("= ? μm");


		JLabel mlabel = new JLabel("Tissue dimensions (μm): ");

		JPanel magPanel = new JPanel();
		magPanel.setLayout(new BoxLayout(magPanel, BoxLayout.LINE_AXIS));
		yResSpinner.setMaximumSize(new Dimension(80, yResSpinner.getPreferredSize().height));
		magPanel.add(yResSpinner);
		JLabel um = new JLabel("x");
		magPanel.add(um);

		magPanel.add(xResSpinner);


		distancePixelSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				double pixels = (double) distancePixelSpinner.getValue();
				double rx = pxSize.x / (double) xResSpinner.getValue();
				double ry = pxSize.y / (double) yResSpinner.getValue(); 
				double res = (rx + ry) / 2;
				double conv = pixels / res;

				umResult.setText(String.format("= %.2f μm", conv));

			}
		});
		xResSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				double x = (double) xResSpinner.getValue();
				
				
				double rx = pxSize.x / x;
				double ry = pxSize.y / (double) yResSpinner.getValue(); 
				double res = (rx + ry) / 2;
				double pixels = (double) distancePixelSpinner.getValue();
				double conv = pixels / res;

				umResult.setText(String.format("= %.2f μm", conv));

			}
		});
		yResSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				double y = (double) yResSpinner.getValue();
				
				
				double rx = pxSize.x / (double) xResSpinner.getValue();
				double ry = pxSize.y / y; 
				double res = (rx + ry) / 2;
				double pixels = (double) distancePixelSpinner.getValue();
				double conv = pixels / res;
				umResult.setText(String.format("= %.2f μm", conv));
			}
		});

		add(helper.makePanel("Distance control", distancePanel, umResult, mlabel, magPanel), helper.makeConstraints());

		/********************
		 * REGION SELECTION *
		 ********************/
		JRadioButton useSlider = new JRadioButton("Use auto-sliding window");
		JRadioButton useManual = new JRadioButton("Select manually");

		useManual.setSelected(true);

		ButtonGroup selectionGroup = new ButtonGroup();
		selectionGroup.add(useSlider);
		selectionGroup.add(useManual);

		JPanel subPanel = new JPanel();
		subPanel.setLayout(new BoxLayout(subPanel, BoxLayout.LINE_AXIS));
		subPanel.add(useSlider);
		subPanel.add(useManual);

		JPanel spinnerPanel = new JPanel();
		spinnerPanel.setLayout(new FlowLayout());
		JLabel text = new JLabel("Autoslider dimensions: ");
		spinnerPanel.add(text);
		text.setEnabled(false);
		JSpinner x = new JSpinner(new SpinnerNumberModel(2, 1, 10, 1));
		x.setEnabled(false);
		JSpinner y = new JSpinner(new SpinnerNumberModel(2, 1, 10, 1));
		y.setEnabled(false);
		spinnerPanel.add(x);
		JLabel mid = new JLabel("x");
		mid.setEnabled(false);
		spinnerPanel.add(mid);
		spinnerPanel.add(y);

		JPanel overlapPanel = new JPanel();
		overlapPanel.setLayout(new FlowLayout());
		JLabel overlapText = new JLabel("Window overlap percent: ");
		overlapText.setEnabled(false);
		JSpinner overlapSpinner = new JSpinner(new SpinnerNumberModel(10d, 0d, 100d, 1d));
		overlapPanel.add(overlapText);
		overlapPanel.add(overlapSpinner);
		overlapSpinner.setMaximumSize(new Dimension(80, overlapSpinner.getPreferredSize().height));
		overlapSpinner.setEnabled(false);

		useSlider.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				boolean set = useSlider.isSelected();
				x.setEnabled(set);
				y.setEnabled(set);
				mid.setEnabled(set);
				text.setEnabled(set);
				overlapText.setEnabled(set);
				overlapSpinner.setEnabled(set);
			}
		});

		JCheckBox useFullBackground = new JCheckBox("Use full dataset as background ");
		useFullBackground.setSelected(true);

		// TODO: Add configuration for slider here, give visual feedback in selection
		// window
		add(helper.makePanel("Region Selection", subPanel, spinnerPanel, overlapPanel, useFullBackground),
				helper.makeConstraints());

		/************************
		 * SIGNIFICANCE CONTROL *
		 ************************/

		String[] tests = new String[] { "Label shuffle", "Hypergeometric test", "No Filtering" };
		JComboBox<String> testBox = new JComboBox<String>(tests);
		testBox.setSelectedItem(tests[0]);

		JPanel interactionPanel = new JPanel();
		interactionPanel.setLayout(new BoxLayout(interactionPanel, BoxLayout.LINE_AXIS));
		JLabel filtLabel = new JLabel("Filter by: ");
		interactionPanel.add(filtLabel);
		String[] interactions = new String[] { "More than expected", "Less than expected", "Anything unexpected" };
		JComboBox<String> interactionBox = new JComboBox<String>(interactions);
		interactionBox.setSelectedItem(interactions[0]);
		interactionPanel.add(interactionBox);

		JPanel pvaluePanel = new JPanel();
		pvaluePanel.setLayout(new BoxLayout(pvaluePanel, BoxLayout.LINE_AXIS));
		JSpinner pvalueSpinner = new JSpinner(new SpinnerNumberModel(0.05d, 0d, 1, 0.01d));
		pvalueSpinner.setMaximumSize(new Dimension(80, pvalueSpinner.getPreferredSize().height));
		JLabel pvalLabel = new JLabel("Condition: p < ");
		pvaluePanel.add(pvalLabel);
		pvaluePanel.add(pvalueSpinner);

		JPanel zPanel = new JPanel();
		zPanel.setLayout(new BoxLayout(zPanel, BoxLayout.LINE_AXIS));

		JSpinner maxZSpinner = new JSpinner(new SpinnerNumberModel(20d, 0d, 100d, 1d));
		maxZSpinner.setMaximumSize(new Dimension(80, pvalueSpinner.getPreferredSize().height));
		maxZSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				double maxZ = (double) maxZSpinner.getValue();
				main.setMaxZ(maxZ);
			}
		});
		zPanel.add(new JLabel("Z-score for max edge thickness: "));
		zPanel.add(maxZSpinner);

		JCheckBox useCorrectionCheckBox = new JCheckBox("Use Bonferroni correction ");
		useCorrectionCheckBox.setSelected(true);

		testBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				boolean set = !(testBox.getSelectedItem() == tests[2]);
				filtLabel.setEnabled(set);
				interactionBox.setEnabled(set);
				pvalueSpinner.setEnabled(set);
				useCorrectionCheckBox.setEnabled(set);
				pvalLabel.setEnabled(set);
			}
		});
		add(helper.makePanel("Significance filtering", testBox, interactionPanel, pvaluePanel, zPanel,
				useCorrectionCheckBox), helper.makeConstraints());

		/********************************
		 * BIG 'OL MAKE-NETWORKS BUTTON *
		 ********************************/
		JButton generateNetworks = new JButton("<html><body><b>Generate network(s)</b></body></html>");
		generateNetworks.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				main.generateNetwork();
			}
		});

		add(generateNetworks, helper.makeConstraints());

		stateManager = new StateManager() {
			public void setControls(ControlSet controls) {
				list.setModel(controls.model);
				list.setSelectedIndex(controls.selected);
				makeNew.setSelected(controls.overwrite);
				testBox.setSelectedIndex(controls.testIndex);
				distancePixelSpinner.setValue(controls.searchDistancePixels);
				xResSpinner.setValue(controls.dx);
				xResSpinner.setValue(controls.dy);
				if (controls.useManual)
					useManual.setSelected(true);
				else
					useSlider.setSelected(true);
				pxSize.setLocation(controls.pxSize);
				x.setValue(controls.x);
				y.setValue(controls.y);
				overlapSpinner.setValue(controls.overlap);
				useFullBackground.setSelected(controls.useBackground);
				interactionBox.setSelectedIndex(controls.interactionIndex);
				pvalueSpinner.setValue(controls.pvalue);
				useCorrectionCheckBox.setSelected(controls.doCorrection);
				maxZSpinner.setValue(controls.maxZ);
			}

			public ControlSet saveControls() {
				ControlSet state = new ControlSet();
				state.model = (DefaultListModel<ListedNetwork>) list.getModel();
				state.selected = list.getSelectedIndex();
				state.overwrite = makeNew.isSelected();
				state.testIndex = testBox.getSelectedIndex();
				state.searchDistancePixels = (double) distancePixelSpinner.getValue();
				state.pxSize = (Point2D.Double) pxSize.clone();
				state.dx = (double) xResSpinner.getValue();
				state.dy = (double) xResSpinner.getValue();
				state.useManual = useManual.isSelected();
				state.x = (int) x.getValue();
				state.y = (int) y.getValue();
				state.overlap = (double) overlapSpinner.getValue();
				state.useBackground = useFullBackground.isSelected();
				state.interactionIndex = interactionBox.getSelectedIndex();
				state.pvalue = (double) pvalueSpinner.getValue();
				state.doCorrection = useCorrectionCheckBox.isSelected();
				state.maxZ = (double) maxZSpinner.getValue();
				return state;
			}

			/**
			 * In the case that there is a need to make a new dataset
			 */
			public ControlSet newFromCurrent(double width, double height) {
				ControlSet state = new ControlSet();
				state.model = new DefaultListModel<>();
				state.selected = 0;
				state.overwrite = makeNew.isSelected();
				state.testIndex = testBox.getSelectedIndex();
				state.searchDistancePixels = (double) distancePixelSpinner.getValue();
				state.dx = (double) xResSpinner.getValue();
				state.dy = (double) xResSpinner.getValue();
				
				state.pxSize = new Point2D.Double(width, height);
				double pixels = (double) distancePixelSpinner.getValue();
				double rx = width / (double) xResSpinner.getValue();
				double ry = height / (double) yResSpinner.getValue(); 
				double res = (rx + ry) / 2;
				double conv = pixels / res;

				umResult.setText(String.format("= %.2f μm", conv));

				state.useManual = useManual.isSelected();
				state.x = (int) x.getValue();
				state.y = (int) y.getValue();
				state.overlap = (double) overlapSpinner.getValue();
				state.useBackground = useFullBackground.isSelected();
				state.interactionIndex = interactionBox.getSelectedIndex();
				state.pvalue = (double) pvalueSpinner.getValue();
				state.doCorrection = useCorrectionCheckBox.isSelected();
				state.maxZ = (double) maxZSpinner.getValue();
				return state;
			}
		};
	}

	/*
	 * Call this to apply a saved ControlSet to the panel controls
	 */
	public void loadState(ControlSet controls) {
		stateManager.setControls(controls);
	}

	/*
	 * Saves the configuration setup as a ControlSet
	 */
	public ControlSet saveState() {
		return stateManager.saveControls();
	}

	public ControlSet newState(double x, double y) {
		return stateManager.newFromCurrent(x, y);
	}
}
