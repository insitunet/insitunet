package org.cytoscape.insitunet.internal.panel;

import java.awt.geom.Point2D;
import java.io.Serializable;

import javax.swing.DefaultListModel;

import org.cytoscape.insitunet.internal.typenetwork.ListedNetwork;
import org.cytoscape.view.layout.CyLayoutAlgorithm;

/*
 * Just a big container that can be passed around containing the state
 * of the ControlPanel
 */
public class ControlSet implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1633859642294311200L;
	// TODO: This is awesome but the algorithm shouldn't be data dependent, figure
	// it out when you add newsync
	transient public CyLayoutAlgorithm algorithm;
	transient public DefaultListModel<ListedNetwork> model; // list of the networks
	
	public int selected; // currently selected network

	public boolean overwrite;

	public int testIndex; // test type string

	// distance panel
	public double searchDistancePixels;
	public double dx, dy; //um
	public Point2D.Double pxSize; // px

	// region selection
	public boolean useManual;
	public int x;
	public int y;

	// significance
	public int interactionIndex;
	public double pvalue;
	public boolean doCorrection;

	public boolean useBackground;
	protected double maxZ;
	public double overlap;

}
