package org.cytoscape.insitunet.internal.panel;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cytoscape.insitunet.internal.Gene;
import org.cytoscape.insitunet.internal.InsituDataset;
import org.cytoscape.insitunet.internal.Symbol;
import org.cytoscape.insitunet.internal.gl.GLPanel;
import org.cytoscape.insitunet.internal.typenetwork.Transcript;

public class StylePanel implements ItemListener, ChangeListener {

	final GLPanel panel;

	final List<Gene> genes;
	final JFrame frame = new JFrame("Style Control");
	final JColorChooser chooser = new JColorChooser();
	final JSlider geneScaleSlider = new JSlider(10, 400, 100);
	final JSlider masterScaleSlider = new JSlider(10, 1000, 400);

	final DefaultComboBoxModel<Gene> geneComboModel = new DefaultComboBoxModel<Gene>();
	final JComboBox<Gene> comboBox = new JComboBox<Gene>(geneComboModel);
	final InsituDataset dataset;

	final List<SymbolTile> tiles = new ArrayList<>();

	public StylePanel(GLPanel panel, InsituDataset dataset, JFrame windowFrame) {
		this.panel = panel;
		this.dataset = dataset;
		this.genes = dataset.getGenes();
		comboBox.addItemListener(this);
		chooser.setPreviewPanel(new JPanel());
		for (AbstractColorChooserPanel chooserPanel : chooser.getChooserPanels()) {
			if (chooserPanel.getDisplayName().equals("HSV")) {
				chooser.setChooserPanels(new AbstractColorChooserPanel[] { chooserPanel });
				break;
			}
		}
		chooser.getSelectionModel().addChangeListener(this);
		chooser.setBorder(BorderFactory.createTitledBorder("Selected Gene Colour"));
		masterScaleSlider.addChangeListener(this);
		geneScaleSlider.addChangeListener(this);
		JPanel tilePanel = new JPanel();
		tilePanel.setLayout(new BoxLayout(tilePanel, BoxLayout.X_AXIS));
		tilePanel.setBorder(BorderFactory.createTitledBorder("Symbol Shape"));
		for (Gene gene : genes) {
			geneComboModel.addElement(gene);
		}
		ButtonGroup group = new ButtonGroup();
		for (Symbol symbol : panel.getProgram().getSymbolList()) {
			SymbolTile tile = new SymbolTile(symbol);
			tilePanel.add(tile);
			tiles.add(tile);
			group.add(tile);
		}
		tilePanel.add(Box.createHorizontalGlue()); // Pads the panel out
		JPanel tileScalePanel = new JPanel();
		tileScalePanel.setLayout(new BoxLayout(tileScalePanel, BoxLayout.PAGE_AXIS));

		tileScalePanel.add(tilePanel);
		tileScalePanel.add(geneScaleSlider);
		geneScaleSlider.setBorder(BorderFactory.createTitledBorder("Selected Gene Symbol Scale"));
		tileScalePanel.add(masterScaleSlider);
		masterScaleSlider.setBorder(BorderFactory.createTitledBorder("Master Symbol Scale"));
		frame.setMinimumSize(new Dimension(300, 240));
		frame.setAlwaysOnTop(true);
		Container content = frame.getContentPane();
		content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
		content.add(comboBox);
		Component strut = Box.createVerticalStrut(5);
		strut.setPreferredSize(new Dimension(10, 5));
		content.add(strut);
		content.add(comboBox);
		content.add(chooser);
		content.add(tileScalePanel);
		frame.pack();
		frame.setLocationRelativeTo(windowFrame);

		Gene gene = (Gene) geneComboModel.getSelectedItem();
		chooser.setColor(gene.getColor());
		setSelectedSymbol(gene.getSymbol());
		geneScaleSlider.setValue((int) (gene.getScale() * 100));
	}

	/**
	 * Shows the StylePanel if closed or hidden
	 */
	public void show(Transcript t) {
		if (t != null) {
			geneComboModel.setSelectedItem(t.getGene());
		}
		frame.setVisible(true);
	}

	/**
	 * Is called when a SymbolTile is manually selected.
	 */
	private void symbolButtonPressed(Symbol symbol) {
		Gene gene = (Gene) geneComboModel.getSelectedItem();
		gene.setSymbol(symbol);
		panel.go();
	}

	/**
	 * A JToggleButton that represents a particular Symbol.
	 */
	private class SymbolTile extends JToggleButton {

		private static final long serialVersionUID = -5160591855440897627L;
		public final Symbol symbol;

		public SymbolTile(Symbol symbol) {
			super(new ImageIcon(symbol.makeImage(new Dimension(30, 30))));
			this.symbol = symbol;
			setPreferredSize(new Dimension(40, 40));
			addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					// SymbolTile pressed
					symbolButtonPressed(symbol);
				}
			});
		}

	}

	/**
	 * Ensures the provided Symbol's SymbolTile is selected.
	 */
	private void setSelectedSymbol(Symbol symbol) {
		for (SymbolTile tile : tiles) {
			if (tile.symbol == symbol) {
				tile.setSelected(true);
			}
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		// A different Gene is selected in the Gene JComboBox
		Gene gene = (Gene) geneComboModel.getSelectedItem();
		chooser.setColor(gene.getColor());
		setSelectedSymbol(gene.getSymbol());
		geneScaleSlider.setValue((int) (gene.getScale() * 100));
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		Gene gene = (Gene) geneComboModel.getSelectedItem();
		if (e.getSource() == geneScaleSlider) {
			float scale = geneScaleSlider.getValue() / 100f;
			gene.setScale(scale);
		} else if (e.getSource() == masterScaleSlider) {
			float scale = masterScaleSlider.getValue() / 100f;
			panel.setSymbolMasterScale(scale);
		} else {
			// The colour has been changed
			gene.setColor(chooser.getColor());
			dataset.updateGeneStyle(gene);

		}
		panel.go();
	}

}
