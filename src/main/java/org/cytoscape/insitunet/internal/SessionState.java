package org.cytoscape.insitunet.internal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;

import org.cytoscape.insitunet.internal.panel.MainPanel;
import org.cytoscape.insitunet.internal.panel.RehydrationPack;
import org.cytoscape.insitunet.internal.panel.SelectionPanel;
import org.cytoscape.insitunet.internal.typenetwork.ListedNetwork;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.session.events.SessionAboutToBeSavedEvent;
import org.cytoscape.session.events.SessionAboutToBeSavedListener;
import org.cytoscape.session.events.SessionLoadedEvent;
import org.cytoscape.session.events.SessionLoadedListener;
import org.cytoscape.view.model.CyNetworkView;

public class SessionState  implements SessionAboutToBeSavedListener, SessionLoadedListener {
	
	InsituNetActivator activator;
	
	public SessionState(InsituNetActivator a) {
		activator = a;
	}
	
	public void handleEvent(SessionLoadedEvent e) {

		if (e.getLoadedSession().getAppFileListMap() == null || e.getLoadedSession().getAppFileListMap().size() == 0) {
			return;
		}
		List<File> files = e.getLoadedSession().getAppFileListMap().get("insitunet-save");
		
		activator.restart();

		try {			
			File propFile = files.get(0);	
			FileInputStream fis = new FileInputStream(propFile);
			ObjectInputStream ois = new ObjectInputStream(fis);			
			ArrayList<InsituDataset> datasets = (ArrayList<InsituDataset>) ois.readObject();
			ois.close();
			
			File listOfListedNetworkLists = files.get(1); //lol
			fis = new FileInputStream(listOfListedNetworkLists);
			ois = new ObjectInputStream(fis);
			ArrayList<ArrayList<ListedNetwork>> listyMcGee = (ArrayList<ArrayList<ListedNetwork>>) ois.readObject();
			
			activator.getCAA().getCyEventHelper().flushPayloadEvents();
			
			for(int i = 0; i < datasets.size(); i++) {

				InsituDataset dataset = datasets.get(i);
				ArrayList<ListedNetwork> list = listyMcGee.get(i);
				dataset.activator = activator;

				dataset.controls.model = new DefaultListModel<>();
				for(ListedNetwork net : list) {
					net.rehydrate(e.getLoadedSession(), dataset);
					dataset.controls.model.addElement(net);
				}
				dataset.filteredNetwork.rehydrate(activator.getCAA(), list);
			}
			
			File rpfile = files.get(2); //lol
			fis = new FileInputStream(rpfile);
			ois = new ObjectInputStream(fis);
			RehydrationPack rp = (RehydrationPack) ois.readObject();
			
			activator.getPanel().enabled = false;
			activator.getPanel().unionNetwork =  e.getLoadedSession().getObject(rp.networkSUID, CyNetwork.class);
			for(int i = 0; i < rp.positions.length; i++) {
				int[] pos = rp.positions[i];
				ListedNetwork element = listyMcGee.get(pos[0]).get(pos[1]);
				activator.getPanel().listModel.add(i, element);
			}
			activator.getPanel().rehydrateNodeMap();
			
			// Make new session by calling activator.
			for (InsituDataset dataset : datasets) {
				activator.serializedImport(dataset);
			}
			activator.getPanel().enabled = true;

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public ArrayList<ListedNetwork> modelToList(DefaultListModel<ListedNetwork> model) {
		ArrayList<ListedNetwork> list = new ArrayList<>();
		for (int i = 0; i < model.size(); i++) {
			list.add(model.getElementAt(i));
		}
		return list;
	}

	@Override
	public void handleEvent(SessionAboutToBeSavedEvent e) {

		String dir = System.getProperty("java.io.tmpdir");
		File propFile = new File(dir, "insitunet-save.props");
		File listFile = new File(dir, "insitunet-lists.props");
		File positionsFile = new File(dir, "insitunet-pos.props");
		
		try {
			FileOutputStream fos = new FileOutputStream(propFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			ArrayList<InsituDataset> datasets = activator.getPanel().getDatasets();
			oos.writeObject(datasets);
			oos.close();

			fos = new FileOutputStream(listFile);
			oos = new ObjectOutputStream(fos);
			
			ArrayList<ArrayList<ListedNetwork>> listOfLists = new ArrayList<>();
			for (InsituDataset dataset : datasets) {
				listOfLists.add(modelToList(dataset.controls.model));
			}
			int i = 0, k = 0;
			int[][] positions = new int[activator.getPanel().getLNLength()][2];
			for (ArrayList<ListedNetwork> list : listOfLists) {
				for (ListedNetwork ln : list) {
					int pos = activator.getPanel().getLNPosition(ln);
					if(pos >= 0) {
						positions[pos] = new int[] {i, k};
					}
					k++;
				}
				i++;
			}
			oos.writeObject(listOfLists);
			oos.close();
			
			fos = new FileOutputStream(positionsFile);
			oos = new ObjectOutputStream(fos);
			RehydrationPack rp = activator.getPanel().getRehydrationPack();
			rp.positions = positions;
			oos.writeObject(rp);
			oos.close();

			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		ArrayList<File> files = new ArrayList<File>();
		files.add(propFile);
		files.add(listFile);
		files.add(positionsFile);
		try {
			e.addAppFiles("insitunet-save", files);			
		}
		catch (Exception ex){
			ex.printStackTrace();
}	}
	
}
