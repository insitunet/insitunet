package org.cytoscape.insitunet.internal.util;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import org.cytoscape.util.swing.BasicCollapsiblePanel;

public class LayoutHelper {

	private int constraintCounter = 0;

	public GridBagConstraints makeConstraints() {
		GridBagConstraints cons = new GridBagConstraints();
		cons.gridy = constraintCounter++;
		cons.weightx = 1;
		cons.weighty = 1;
		cons.anchor = GridBagConstraints.NORTHWEST;
		cons.fill = GridBagConstraints.HORIZONTAL;
		cons.insets = new Insets(4, 8, 4, 8);
		return cons;
	}

	public BasicCollapsiblePanel makePanel(String title, Component... components) {
		BasicCollapsiblePanel bcp = new BasicCollapsiblePanel(title);
		bcp.getContentPane().setLayout(new GridBagLayout());

		int i = 0;
		for (Component c : components) {
			GridBagConstraints cons = new GridBagConstraints();
			cons.gridy = i++;
			cons.weightx = 1;
			cons.anchor = GridBagConstraints.NORTHWEST;
			cons.fill = GridBagConstraints.BOTH;
			cons.insets = new Insets(4, 4, 4, 4);
			bcp.add(c, cons);
		}

		bcp.setCollapsed(true); // Start fully collapsed

		return bcp;
	}

}