package org.cytoscape.insitunet.internal.gl;

import java.io.Serializable;

/**
 * A basic 2D point.
 */
public class Point2D implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5613119798461227757L;
	public final float x;
	public final float y;

	public Point2D(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Point2D(double x, double y) {
		this.x = (float) x;
		this.y = (float) y;
	}

	/**
	 * Find the midpoint of this and the given point.
	 */
	public Point2D midpoint(Point2D other) {
		return new Point2D(this.x - (this.x - other.x) / 2.0f, this.y - (this.y - other.y) / 2.0f);
	}

	/**
	 * Calculate euclidean distance from this point to another.
	 */
	public double euclideanDistance(Point2D other) {
		double sqrdist = Math.pow((this.x - other.x), 2) + Math.pow((this.y - other.y), 2);
		return Math.sqrt(sqrdist);
	}

	public boolean equals(Point2D other) {
		Float x_obj = x, y_obj = y;
		if (x_obj.compareTo(other.x) != 0)
			return false;
		if (y_obj.compareTo(other.y) != 0)
			return false;
		return true;
	}

}