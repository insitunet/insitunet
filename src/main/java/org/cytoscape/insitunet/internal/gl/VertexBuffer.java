package org.cytoscape.insitunet.internal.gl;

import com.jogamp.opengl.GL3;

/**
 * A basic interface for a VertexBuffer. In this model, the VertexBuffer
 * implementation also defines how rendering happens. We can then iterate over
 * multiple implementations of VertexBuffer to render different things. Also
 * contains some static methods for generating specific geometry.
 * 
 * @author John Salamon
 *
 */
public interface VertexBuffer {

	/**
	 * Does whatever is required to write to this VertexBuffer's buffer(s).
	 */
	public void bufferData(GL3 gl);

	/**
	 * Sets up and/or generates all vertex array objects. Should be called once,
	 * preferably at init.
	 */
	public void genVertexArray(GL3 gl);

	/**
	 * Draws the contents of the VertexBuffer.
	 */
	public void render(GL3 gl);

	/**
	 * Given two points, creates a quad between them made of two triangles.
	 * 
	 * @param x1
	 *            X coordinate of the first point.
	 * @param y1
	 *            Y coordinate of the first point.
	 * @param x2
	 *            X coordinate of the second point.
	 * @param y2
	 *            Y coordinate of the second point.
	 * @return
	 */
	public static float[] makeQuad(float x1, float y1, float x2, float y2) {
		float[] quad = new float[] { x2, y1, x2, y2, x1, y2, x2, y1, x1, y2, x1, y1 };
		return quad;
	}

	/**
	 * Creates a circle centered on (0,0).
	 * 
	 * @param size
	 *            The number of vertices this circle should have.
	 * @param radius
	 *            What radius should the circle be.
	 * @return
	 */
	public static float[] makeCircle(int size, float radius) {
		float[] circle = new float[size];
		double step = (2 * Math.PI) / (circle.length / 2);
		double angle = 0;
		for (int i = 0; i < circle.length; i += 2) {
			circle[i] = (float) (radius * Math.cos(angle));
			circle[i + 1] = (float) (radius * Math.sin(angle));
			angle += step;
		}
		return circle;
	}
}
