package org.cytoscape.insitunet.internal.gl;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.InputStream;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

/**
 * An attempt at drawing 2D text
 * 
 * @author jrs
 *
 */
public class TextRenderer {

	final Font font;

	public TextRenderer(GL3 gl, String name, int fontSize) {

		InputStream stream = TextRenderer.class.getResourceAsStream("/font/" + name + ".ttf");
		Font font;

		try {
			Font raw = Font.createFont(Font.TRUETYPE_FONT, stream);
			font = raw.deriveFont(Font.PLAIN, fontSize);
			stream.close();
		} catch (Exception e) {
			font = new Font(name, Font.PLAIN, fontSize);
			e.printStackTrace();
		}

		this.font = font;
	}

	public static Texture zeroTexture() {
		BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = image.createGraphics();
		g2d.setBackground(new Color(0, 0, 0, 0));
		g2d.clearRect(0, 0, 1, 1);
		g2d.dispose();
		return AWTTextureIO.newTexture(GLProfile.get(GLProfile.GL3), image, false);
	}

	/**
	 * Creates a Texture containing the specified text
	 */
	public Texture stringToTexture(String text) {

		BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = image.createGraphics();

		g2d.setFont(font);
		FontMetrics fm = g2d.getFontMetrics();
		int width = fm.stringWidth(text);
		int height = fm.getHeight();
		g2d.dispose();

		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		g2d = image.createGraphics();
		g2d.setBackground(new Color(0, 0, 0, 0));
		g2d.clearRect(0, 0, width, height);

		g2d.setFont(font);
		fm = g2d.getFontMetrics();
		g2d.setColor(Color.BLACK);
		g2d.drawString(text, 0, fm.getAscent());
		g2d.dispose();

		return AWTTextureIO.newTexture(GLProfile.get(GLProfile.GL3), image, false);
	}

}