package org.cytoscape.insitunet.internal.gl;

import java.util.concurrent.atomic.AtomicBoolean;

import com.jogamp.opengl.GLAutoDrawable;

/**
 * Runs the OpenGL context in a separate thread.
 *
 * @author John Salamon
 */
public class Animator implements Runnable {

	private final GLAutoDrawable drawable;

	private Thread thread;

	final AtomicBoolean needToDraw = new AtomicBoolean(false);

	private boolean running;

	public Animator(GLAutoDrawable drawable) {
		this.drawable = drawable;
		this.running = true;
	}

	public void run() {
		while (running) {

			// Keep the thread drawing while we still need to
			while (needToDraw.get()) {
				needToDraw.set(false);
				drawable.display();
			}

			// Continuous drawing is done for now, the thread can wait
			synchronized (needToDraw) {
				try {
					needToDraw.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * External thread will call this, notify()-ing the needToDraw object.
	 */
	public void go() {
		needToDraw.set(true);
		synchronized (needToDraw) {
			needToDraw.notify();
		}
	}

	/**
	 * Start the animator by creating a new thread.
	 */
	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	public void stop() {
		running = false;
	}
}
