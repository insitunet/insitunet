package org.cytoscape.insitunet.internal.gl;

import java.io.Serializable;
import java.nio.FloatBuffer;
import java.util.ArrayList;

/**
 * An ArrayList-backed polygonal 2D shape.
 */
public class Shape2D implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8566788162487264360L;
	ArrayList<Point2D> vertices = new ArrayList<>();
	private boolean complete = false; // A flag to indicate the shape is complete

	public Shape2D(Shape2D shape) {
		for (int i = 0; i < shape.size(); i++) {
			this.push(shape.get(i));
		}
	}

	public Shape2D() {
	}

	/**
	 * Add a new vertex to this shape.
	 * 
	 * @param point
	 *            The vertex to add.
	 */
	public void push(Point2D point) {
		vertices.add(point);
	}

	public Point2D getLast() {
		return vertices.get(vertices.size() - 1);
	}

	public void push(float x, float y) {
		vertices.add(new Point2D(x, y));
	}

	public void setComplete(boolean b) {
		complete = b;
	}

	public boolean isComplete() {
		return complete;
	}

	/**
	 * Make rectangle shapes more easily
	 */
	public void rectanglePush(Point2D opposite, Renderer r) {
		Point2D origin = get(0);

		// Convert from actual dataset coordinates to screen coordinates
		Point2D screen0 = r.disinvertCoordinates(origin.x, origin.y);
		Point2D screen1 = r.disinvertCoordinates(opposite.x, opposite.y);

		// Get the other two points in the rectangle, and convert back to dataset
		// coordinates
		Point2D r1 = r.invertCoordinates(screen0.x + (screen1.x - screen0.x), screen0.y);
		Point2D r2 = r.invertCoordinates(screen0.x, screen0.y + (screen1.y - screen0.y));

		reset();
		push(origin);
		push(r1);
		push(opposite);
		push(r2);
		push(origin); // close it off (because we draw with GL_LINE_STRIP)
	}

	/**
	 * Replaces the last added vertex with this one.
	 */
	public void replaceLast(float x, float y) {
		vertices.set(vertices.size() - 1, new Point2D(x, y));
	}

	public void replaceLast(Point2D point) {
		vertices.set(vertices.size() - 1, point);
	}

	/**
	 * Delete all vertices in this shape.
	 */
	public void reset() {
		vertices.clear();
	}

	/**
	 * Get a particular vertex index.
	 * 
	 * @param i
	 *            The index to retrieve
	 * @return The vertex at i
	 */
	public Point2D get(int i) {
		return vertices.get(i);
	}

	/**
	 * @return The number of vertices in this shape.
	 */
	public int size() {
		return vertices.size();
	}

	/**
	 * Returns a Shape2D that represents the bounding rectangle for this shape.
	 */
	public Shape2D getBoundingRectangle() {

		// Return infinitely large rectangle if undefined
		if (vertices.size() < 1) {
			Shape2D rectangle = new Shape2D();
			rectangle.push(new Point2D(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY));
			rectangle.push(new Point2D(Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY));
			rectangle.push(new Point2D(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY));
			rectangle.push(new Point2D(Float.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY));
			return rectangle;
		}

		float minimumX = vertices.get(0).x;
		float maximumX = vertices.get(0).x;

		float minimumY = vertices.get(0).y;
		float maximumY = vertices.get(0).y;

		for (int i = 1; i < vertices.size(); i++) {
			Point2D p = vertices.get(i);
			if (p.x > maximumX) {
				maximumX = p.x;
			} else if (p.x < minimumX) {
				minimumX = p.x;
			}
			if (p.y > maximumY) {
				maximumY = p.y;
			} else if (p.y < minimumY) {
				minimumY = p.y;
			}
		}

		Shape2D rectangle = new Shape2D();
		rectangle.push(new Point2D(minimumX, minimumY));
		rectangle.push(new Point2D(minimumX, maximumY));
		rectangle.push(new Point2D(maximumX, maximumY));
		rectangle.push(new Point2D(maximumX, minimumY));
		return rectangle;
	}

	/**
	 * Tests if this shape contains a given point.
	 * http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
	 * 
	 * @param point
	 *            The point to check
	 * @return true if the point is inside the boundary, false otherwise
	 */
	public boolean contains(Point2D point) {
		int i, j;
		boolean result = false;

		if (vertices.size() < 1)
			return true; // assume no boundary if undefined

		for (i = 0, j = size() - 1; i < size(); j = i++) {
			if ((get(i).y > point.y) != (get(j).y > point.y)
					&& (point.x < (get(j).x - get(i).x) * (point.y - get(i).y) / (get(j).y - get(i).y) + get(i).x)) {
				result = !result;
			}
		}
		return result;
	}

	/**
	 * Print a list of all the vertices in this shape.
	 */
	public void print() {
		for (Point2D p : vertices) {
			System.out.println(p.x + ", " + p.y);
		}
	}

	/**
	 * Makes a basic float array and wraps it in a FloatBuffer. Useful for uploading
	 * shape vertices to OpenGL.
	 * 
	 * @return the FloatBuffer
	 */
	public FloatBuffer getBuffer() {
		float[] array = new float[vertices.size() * 2];
		int index = 0;
		for (Point2D p : vertices) {
			array[index++] = p.x;
			array[index++] = p.y;
		}
		return FloatBuffer.wrap(array);
	}

	public boolean equals(Shape2D other) {
		if (other == null)
			return false;
		if (other.size() != this.size())
			return false;
		for (int i = 0; i < size(); i++) {
			if (!this.get(i).equals(other.get(i))) {
				return false;
			}
		}
		return true;

	}
}