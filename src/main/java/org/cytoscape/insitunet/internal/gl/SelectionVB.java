package org.cytoscape.insitunet.internal.gl;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.util.GLBuffers;

public class SelectionVB implements VertexBuffer {

	final Integer selection_vbo, background_vbo, zero_vbo;
	final IntBuffer vao = GLBuffers.newDirectIntBuffer(2);
	final int elementSize = GLBuffers.SIZEOF_FLOAT;
	final int usage = GL3.GL_DYNAMIC_DRAW;
	final int drawType = GL3.GL_LINE_LOOP;

	FloatBuffer buffer;
	final ProgramData program;

	final float[] background = VertexBuffer.makeQuad(-1, -1, 1, 1);

	final Matrix identity = new Matrix();

	static float[] zero = { 0, 0 };

	static float[] RED = { 1, 0, 0, 1 };
	static float[] GREEN = { 0, 1, 0, 1 };

	float[] lineColour = RED;

	public SelectionVB(GL3 gl, ProgramData program) {
		this.program = program;
		int vbo[] = new int[3];
		gl.glGenBuffers(3, vbo, 0);
		this.selection_vbo = vbo[0];
		this.background_vbo = vbo[1];
		this.zero_vbo = vbo[2];
		this.buffer = FloatBuffer.allocate(0);
		bufferData(gl);
		genVertexArray(gl);
	}

	public void bufferData(GL3 gl) {
		// The GLSL 3.3 spec states that uninitialized attributes are undefined,
		// So I'm uploading zeroes to symbol_geometry when instancing isn't in use.
		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, zero_vbo);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, zero.length * elementSize, FloatBuffer.wrap(zero), usage);

		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, selection_vbo);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, buffer.capacity() * elementSize, buffer, usage);

		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, background_vbo);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, background.length * elementSize, FloatBuffer.wrap(background), usage);
	}

	public void bufferSelection(GL3 gl, FloatBuffer buffer) {
		this.buffer = buffer;
		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, selection_vbo);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, buffer.capacity() * elementSize, buffer, usage);
	}

	public void setComplete(boolean b) {
		lineColour = b ? GREEN : RED;
	}

	public void genVertexArray(GL3 gl) {

		gl.glGenVertexArrays(2, vao);

		int stride = 0;
		int offset = 0;

		gl.glBindVertexArray(vao.get(0));
		{
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, zero_vbo);
			{
				gl.glEnableVertexAttribArray(program.getSymbol());
				gl.glVertexAttribPointer(program.getSymbol(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);

			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, selection_vbo);
			{
				gl.glEnableVertexAttribArray(program.getPoint());
				gl.glVertexAttribPointer(program.getPoint(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
		}
		gl.glBindVertexArray(0);

		gl.glBindVertexArray(vao.get(1));
		{
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, zero_vbo);
			{
				gl.glEnableVertexAttribArray(program.getSymbol());
				gl.glVertexAttribPointer(program.getSymbol(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);

			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, background_vbo);
			{
				gl.glEnableVertexAttribArray(program.getPoint());
				gl.glVertexAttribPointer(program.getPoint(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
		}
		gl.glBindVertexArray(0);
	}

	/**
	 * Draws a thick line around the selection area
	 */
	private void drawSelectionLine(GL3 gl) {

		// TODO: On my intel graphics on Windows only, if the buffer length is zero (no
		// selection), things get slow. I have no idea why.
		gl.glLineWidth(3f);
		gl.glEnable(GL3.GL_LINE_SMOOTH);

		gl.glUniform4f(program.getColour(), lineColour[0], lineColour[1], lineColour[2], lineColour[3]);
		gl.glBindVertexArray(vao.get(0));
		gl.glDrawArrays(GL3.GL_LINE_STRIP, 0, buffer.capacity() / 2);
		gl.glBindVertexArray(0);

		gl.glDisable(GL3.GL_LINE_SMOOTH);
		gl.glLineWidth(1f);

	}

	/**
	 * Shade area outside of any convex polygon See
	 * http://stackoverflow.com/questions/25463015/black-out-everything-outside-a-polygon/25463682#25463682
	 * 
	 * @param gl
	 */
	private void drawSelectionMask(GL3 gl) {

		gl.glEnable(GL.GL_STENCIL_TEST);

		// fill polygon w/ stencil buffer
		gl.glColorMask(false, false, false, false);
		gl.glStencilFunc(GL3.GL_ALWAYS, 1, 0);
		gl.glStencilOp(GL3.GL_KEEP, GL3.GL_KEEP, GL3.GL_INVERT);
		gl.glStencilMask(1);

		gl.glBindVertexArray(vao.get(0));
		gl.glDrawArrays(GL3.GL_TRIANGLE_FAN, 0, buffer.capacity() / 2);
		gl.glBindVertexArray(0);

		gl.glColorMask(true, true, true, true);
		gl.glStencilFunc(GL3.GL_EQUAL, 0, 1);
		gl.glStencilOp(GL3.GL_KEEP, GL3.GL_KEEP, GL3.GL_KEEP);

		gl.glUniformMatrix4fv(program.getModelView(), 1, false, identity.getBuffer());
		gl.glUniformMatrix4fv(program.getProjection(), 1, false, identity.getBuffer());
		{
			float transparency = 0.5f;
			if (buffer.capacity() < 1)
				transparency = 0.0f;
			gl.glUniform4f(program.getColour(), 0.1f, 0.1f, 0.1f, transparency);
			gl.glBindVertexArray(vao.get(1));
			gl.glDrawArrays(GL3.GL_TRIANGLES, 0, background.length / 2);
			gl.glBindVertexArray(0);
		}
		gl.glUniformMatrix4fv(program.getModelView(), 1, false, program.getMVMatrix().getBuffer());
		gl.glUniformMatrix4fv(program.getProjection(), 1, false, program.getPMatrix().getBuffer());

		gl.glDisable(GL.GL_STENCIL_TEST);
	}

	public void render(GL3 gl) {

		gl.glUseProgram(program.getProgram());

		gl.glDisable(GL3.GL_DEPTH_TEST);
		drawSelectionLine(gl);
		drawSelectionMask(gl);
		gl.glEnable(GL3.GL_DEPTH_TEST);

		gl.glUseProgram(0);
	}
}
