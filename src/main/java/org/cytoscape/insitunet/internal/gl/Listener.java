package org.cytoscape.insitunet.internal.gl;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;
import com.jogamp.newt.event.MouseAdapter;
import com.jogamp.newt.event.MouseEvent;

/**
 * JOGL data viewer.
 *
 * @author John Salamon
 */
public class Listener extends MouseAdapter implements KeyListener {

	static final long serialVersionUID = 22l;

	GLPanel panel;

	final int[] mouseDown = new int[2];
	
	int legendWidth = 0;
	boolean show = true;
	final int[] button1 = new int[2];
	final int[] button2 = new int[2];
	final int[] button3 = new int[2];

	boolean controlHeld = false;

	final int[] mouseLocation = new int[2];

	final int[] dragDistance = new int[2];

	public Listener(GLPanel panel) {
		this.panel = panel;
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_F:
			panel.fullScreen();
			break;
		case KeyEvent.VK_PAGE_UP:
			break;
		case KeyEvent.VK_PAGE_DOWN:
			break;
		case KeyEvent.VK_CONTROL:
			controlHeld = true;
			break;
		}
	}

	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_CONTROL:
			controlHeld = false;
			break;
		}
	}
	
	public void setLegendWidth(int w) {
		this.legendWidth = w;
	}

	public void showGUI(boolean show) {
		this.show = show;
	}
	
	@Override
	public void mouseWheelMoved(MouseEvent e) {
		if (show && e.getX() < this.legendWidth) {
			// scroll legend
			panel.legendScrollEvent(e.getRotation()[1]);
		} else {
			panel.zoomEvent(e.getRotation()[1], e.getX(), e.getY());
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		mouseDown[0] = e.getX();
		mouseDown[1] = e.getY();
		switch (controlHeld ? MouseEvent.BUTTON3 : e.getButton()) {
		case MouseEvent.BUTTON1:
			button1[0] = e.getX();
			button1[1] = e.getY();
			break;
		case MouseEvent.BUTTON2:
			button2[0] = e.getX();
			button2[1] = e.getY();
			break;
		case MouseEvent.BUTTON3:
			button3[0] = e.getX();
			button3[1] = e.getY();
			panel.shapeButton(e.getX(), e.getY());
			break;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		switch (controlHeld ? MouseEvent.BUTTON3 : e.getButton()) {
		case MouseEvent.BUTTON1:
			if (button1[0] == e.getX() && button1[1] == e.getY()) {
				panel.clickEvent(button1[0], button1[1], e.getButton());
			}
			break;
		case MouseEvent.BUTTON2:
			// button2[0] = e.getX();
			// button2[1] = e.getY();
			break;
		case MouseEvent.BUTTON3:
			if (button3[0] == e.getX() || button3[1] == e.getY()) {
				panel.shapeReleasedInSameSpot(e.getX(), e.getY());
			} else {
				panel.shapeButtonRelease(e.getX(), e.getY());
			}
			break;
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		mouseLocation[0] = e.getX();
		mouseLocation[1] = e.getY();
		panel.mouseMoved(e.getX(), e.getY());
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		boolean b1 = e.isButtonDown(MouseEvent.BUTTON1);
		for (int b : e.getButtonsDown()) {
			if (controlHeld && b == MouseEvent.BUTTON1) {
				b = MouseEvent.BUTTON3;
				b1 = false;
			}

			switch (b) {
			case MouseEvent.BUTTON1:
				panel.translate(mouseDown[0] - e.getX(), mouseDown[1] - e.getY());
				break;
			case MouseEvent.BUTTON2:
				panel.rotate(button2[0], button2[1], mouseDown[1] - e.getY());
				break;
			case MouseEvent.BUTTON3:
				if (!b1)
					panel.shapeButtonDragged(e.getX(), e.getY());
				break;
			}
		}
		mouseDown[0] = e.getX();
		mouseDown[1] = e.getY();
	}

}
