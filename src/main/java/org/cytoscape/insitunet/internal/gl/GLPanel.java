package org.cytoscape.insitunet.internal.gl;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.nio.IntBuffer;

import org.cytoscape.insitunet.internal.InsituDataset;
import org.cytoscape.insitunet.internal.gl.EventBuffer.Event;
import org.cytoscape.insitunet.internal.gl.EventBuffer.EventType;
import org.cytoscape.insitunet.internal.gl.Renderer.Invoker;
import org.cytoscape.insitunet.internal.panel.SelectionPanel.SelectionState;
import org.cytoscape.insitunet.internal.typenetwork.Transcript;

import com.jogamp.newt.Display;
import com.jogamp.newt.Display.PointerIcon;
import com.jogamp.newt.awt.NewtCanvasAWT;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;

/**
 * JOGL data viewer.
 *
 * @author John Salamon
 */
public class GLPanel extends NewtCanvasAWT implements GLEventListener {

	static final long serialVersionUID = 22l;

	final Renderer renderer;
	final Listener listener;
	final GLWindow window;
	final Shape2D shape = new Shape2D();
	final InsituDataset dataset;
	static double SNAP_DISTANCE = 10.0; // For snapping the polygon complete
	int maxBufSize;
	final EventBuffer eventBuffer;
	final Invoker invoker;

	PointerIcon cursor;
	Display display;
	SelectionState state = SelectionState.RECTANGLE;
	boolean shapeComplete = false; // tracks whether the shape is completely drawn separately from the shape itself
									// (Because we sometimes want to make it LOOK complete even though it actually
									// isn't

	public int getMaxBufSize() {
		return maxBufSize;
	}

	public GLPanel(InsituDataset dataset, boolean useAA) {

		this.dataset = dataset;
		/**
		 * NOTE: GLProfile.get() can take a long time on certain machines I believe this
		 * is because of virus scanning software... There doesn't seem to be much I can
		 * do about it.
		 */
		GLProfile profile = GLProfile.get(GLProfile.GL3);
		GLCapabilities capabilities = new GLCapabilities(profile);
		capabilities.setStencilBits(8);
		// Can enable MSAA if desired
		if (useAA) {
			capabilities.setSampleBuffers(true);
			capabilities.setNumSamples(2);
		}
		window = GLWindow.create(capabilities);
		renderer = new Renderer(window, useAA);
		invoker = renderer.new Invoker();
		eventBuffer = renderer.getEventBuffer();

		display = window.getScreen().getDisplay();
		display.createNative();

		window.addGLEventListener(this);
		listener = new Listener(this);
		window.addMouseListener(listener);
		window.addKeyListener(listener);

		this.setNEWTChild(window);
		this.center();
	}

	public void init(GLAutoDrawable drawable) {
		GL3 gl = drawable.getGL().getGL3();
		renderer.init(gl);
		renderer.initTranscriptBuffers(gl, dataset.getGenes());
		listener.setLegendWidth(renderer.getAverageWidth());
		renderer.initMatrices(gl);

		IntBuffer buf = IntBuffer.allocate(1);
		gl.glGetIntegerv(GL3.GL_MAX_RENDERBUFFER_SIZE, buf);
		maxBufSize = buf.get(0);
	}

	public void display(GLAutoDrawable drawable) {
		renderer.render(drawable.getGL().getGL3(), drawable.getSurfaceWidth(), drawable.getSurfaceHeight());
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		renderer.reshape(drawable.getGL().getGL3(), width, height);
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	public void setSelectionState(SelectionState state) {
		this.state = state;
	}

	public void go() {
		eventBuffer.justGo();
	}

	public ProgramData getProgram() {
		return renderer.getProgram();
	}

	public void fullScreen() {
		window.setFullscreen(!window.isFullscreen());
	}

	public void center() {
		Rectangle2D.Double r = dataset.getDimensions();
		eventBuffer.add(new Event(0, 0, r, EventType.CENTER));
	}

	public void showGUI(boolean show) {
		renderer.showGUI(show);
		listener.showGUI(show);
	}

	public void outputToFile(String path) {
		eventBuffer.add(new Event(window.getWidth(), window.getHeight(), path, EventType.RESHAPE));
	}

	public void outputToFile(String path, int x, int y) {
		eventBuffer.add(new Event(x, y, path, EventType.RESHAPE));
	}

	public void setImage(BufferedImage image) {
		eventBuffer.add(new Event(0, 0, image, EventType.IMAGE));
	}

	public float getImageScale() {
		return renderer.getImageScale();
	}

	public void setImageScale(float scale) {
		renderer.setImageScale(scale);
	}
	
	public void legendScrollEvent(float f) {
		if (Math.abs(f) < 0.1)
			return;
		float factor = f > 0 ? 1f : -1f;
		eventBuffer.add(new Event(0, 0, factor, EventType.LEGEND));
	}

	public void zoomEvent(float f, int x, int y) {
		if (Math.abs(f) < 0.1)
			return;
		float factor = f > 0 ? 1.05f : 0.95f;
		eventBuffer.add(new Event(x, y, factor, EventType.ZOOM));
	}

	public void translate(int x, int y) {
		eventBuffer.add(new Event(x, y));
	}

	/**
	 * The shape button has been pressed
	 */
	public void shapeButton(int x, int y) {
		Point2D vec = renderer.invertCoordinates(x, y);
		switch (state) {
		case RECTANGLE:
			// For a rectangle, we start a new shape every time the shape button is pressed
			shape.reset();
			shape.push(vec); // push origin
			break;
		case POLYGON:
			// For a polygon we only reset if the shape was already complete
			if (shapeComplete || shape.size() == 0) {
				shape.reset();
				shapeComplete = false;
				shape.push(vec); // push a new origin
				shape.push(vec); // push a temp point
			} else {
				// If we didn't just reset the shape we need to test if we're next to the origin
				if (isSnappable(x, y)) {
					Point2D origin = shape.get(0);
					shape.replaceLast(origin); // push a new polygon point
					// shape.push(vec); // push a temp point
					shapeComplete = true;
				} else {
					shape.replaceLast(vec); // push a new polygon point
					shape.push(vec); // push a temp point
				}
			}
			break;
		}
		shape.setComplete(shapeComplete);
		setShape(shape);
	}

	public void setShape(Shape2D shape) {
		eventBuffer.add(new Event(shape.getBuffer(), shape.isComplete()));
	}

	public void shapeButtonRelease(int x, int y) {
		Point2D vec = renderer.invertCoordinates(x, y);

		switch (state) {
		case RECTANGLE:
			shape.rectanglePush(vec, renderer);
			break;
		case POLYGON:
			if (!shapeComplete && shape.size() > 0) {
				if (isSnappable(x, y)) {
					shapeComplete = true;
					shape.push(shape.get(0)); // push a final origin point (because we might not be exactly on the
												// origin)
				}
			}
			break;
		}
		shape.setComplete(shapeComplete);
		setShape(shape);
	}

	/**
	 * Smooth the otherwise highly quanatized line by finding the midpoint of the
	 * current and last point. The more iterations the smoother, but also the
	 * further behind the line will lag.
	 * 
	 * @param last
	 * @param current
	 * @param iterations
	 * @return
	 */
	public Point2D dequantize(Point2D last, Point2D current, int iterations) {
		Point2D dequantized = current;
		for (int i = 0; i < iterations; i++) {
			dequantized = last.midpoint(dequantized);
		}
		return dequantized;
	}

	/**
	 * The shape button has been held and dragged
	 */
	public void shapeButtonDragged(int x, int y) {

		Point2D vec = renderer.invertCoordinates(x, y);

		switch (state) {
		case RECTANGLE:
			shape.rectanglePush(vec, renderer);
			setShape(shape);
			shapeComplete = true;
			shape.setComplete(true);
			break;
		case POLYGON:
			if (!shapeComplete) {
				Point2D last = shape.getLast();
				Point2D dequantized = dequantize(last, vec, 2);

				shape.replaceLast(dequantized); // push a new polygon point
				shape.push(dequantized); // push a temp point

				shape.setComplete(isSnappable(x, y));
				setShape(shape);
			}
			break;
		}
	}

	public boolean isSnappable(int x, int y) {
		Point2D origin = shape.get(0);
		Point2D originScreen = renderer.disinvertCoordinates(origin.x, origin.y); // get actual screen coords of origin
		return originScreen.euclideanDistance(new Point2D(x, y)) < SNAP_DISTANCE;
	}

	public void mouseMoved(int x, int y) {
		Point2D vec = renderer.invertCoordinates(x, y);
		dataset.getPanel().signalPositionChange(vec.x, vec.y);

		switch (state) {
		case RECTANGLE:
			break;
		case POLYGON:
			if (!shapeComplete && shape.size() > 0) {
				if (isSnappable(x, y)) {
					shape.setComplete(true);
				} else {
					shape.setComplete(false);
				}
				shape.replaceLast(vec);
				setShape(shape);
			}
			break;
		}
	}

	public void rotate(int x, int y, float amount) {
		eventBuffer.add(new Event(x, y, amount, EventType.ROTATE));
	}

	public void clickEvent(int x, int y, int button) {
		Point2D vec = renderer.invertCoordinates(x, y);

		/*
		 * if(shape.contains(vec)) {s System.out.println("Contained"); } else {
		 * System.out.println("Free"); }
		 */

		Transcript t = dataset.getNearest(new double[] { vec.x, vec.y });
		Point2D tPoint = renderer.disinvertCoordinates((float) t.getX(), (float) t.getY());
		if (tPoint.euclideanDistance(new Point2D(x, y)) < 20) {
			invoker.setTranscript(t);
		} else {
			invoker.setTranscript(null);
		}
		window.invoke(false, invoker);
	}

	public Transcript getSelected() {
		return invoker.t;
	}

	public void shapeReleasedInSameSpot(int x, int y) {
		switch (state) {
		case RECTANGLE:
			shape.reset();
			setShape(shape);
			break;
		case POLYGON:
			if (shape.size() < 4 && shapeComplete) {
				shape.reset();
				setShape(shape);
			}
			break;
		}
	}

	/**
	 * Get the selected shape
	 * 
	 * @return
	 */
	public Shape2D getShape() {
		return shape;
	}

	public void setShapeAs(Shape2D shape) {
		setShape(shape);
		shape.setComplete(true);
		shapeComplete = true;
	}

	public void setSymbolMasterScale(float scale) {
		renderer.setSymbolMasterScale(scale);
	}

	public void shutDown() {
		eventBuffer.shutDown();
		this.destroy();
	}

	public void setShowAll(boolean selected) {
		renderer.setShowAll(selected);
		go();
	}

}
