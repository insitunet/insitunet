package org.cytoscape.insitunet.internal.gl;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.cytoscape.insitunet.internal.Gene;
import org.cytoscape.insitunet.internal.gl.EventBuffer.Event;
import org.cytoscape.insitunet.internal.gl.EventBuffer.Event.ShapeData;
import org.cytoscape.insitunet.internal.gl.EventBuffer.EventHandler;
import org.cytoscape.insitunet.internal.typenetwork.Transcript;

import com.jogamp.opengl.FBObject;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLRunnable;

/**
 * The scene to be rendered. Contains most OpenGL code.
 * @author John Salamon
 *
 */
public class Renderer implements EventHandler {
    
    // Don't really need separate VBOs, but it makes it easier 
    final List<VertexBuffer> transcriptBuffers = new ArrayList<>(); // One TranscriptVB per transcript type

    SelectionVB selection; // Draws the user selected regions    
    GUIVB guiVB; // Other things like scalebar, legend, compass..
    ImageTileBuffer imageTileBuffer;
    final EventBuffer eventBuffer;
    int width, height;
    final int MSAA_SAMPLES;
    
    final ImageExporter exporter = new ImageExporter();
    ProgramData program = new ProgramData();
    
    public void setImageScale(float scale) {
    	imageTileBuffer.setImageScale(scale);
    	eventBuffer.justGo();
    }
    
    public float getImageScale() {
    	return imageTileBuffer.getImageScale();
    }
    
	Renderer(GLAutoDrawable drawable, boolean useAA) {
		MSAA_SAMPLES = useAA ? 2 : 0;
		this.eventBuffer = new EventBuffer(drawable, this);
	}
	
    protected void init(GL3 gl) {

        // Attempt to enable vsync
        gl.setSwapInterval(1);
        
        exporter.init(gl);
		
        // Enable specific OpenGL capabilities.
        gl.glEnable(GL3.GL_MULTISAMPLE);
        gl.glEnable(GL3.GL_BLEND);
        gl.glEnable(GL3.GL_DEPTH_TEST);
        
        gl.glBlendFunc(GL3.GL_SRC_ALPHA, GL3.GL_ONE_MINUS_SRC_ALPHA);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    }
    public int getAverageWidth() {
    		return guiVB.averageWidth;
    }
    
    public void initTranscriptBuffers(GL3 gl, List<Gene> genes) {
    	transcriptBuffers.clear();
    	program.init(gl, "standard");
		gl.glUseProgram(program.getProgram());

		int i = 0;
    	for(Gene g : genes) {
    		// First x indices correspond exactly to the transcripts.
    		int symbolIndex = i++ % program.getNumSymbols();
    		g.setSymbol(program.getSymbolList().get(symbolIndex));
    		transcriptBuffers.add(new TranscriptVB(gl, g, program));
    	}
    	selection = new SelectionVB(gl, program);
		imageTileBuffer = new ImageTileBuffer(gl, program);
	    transcriptBuffers.add(imageTileBuffer);
    	
    	transcriptBuffers.add(selection);
    	
    	guiVB = new GUIVB(gl, program, genes);
    	transcriptBuffers.add(guiVB);
    }
    
    public ProgramData getProgram() {
    		return program;
    }
    
    public void initMatrices(GL3 gl) {
		gl.glUseProgram(program.getProgram());
		
        gl.glUniformMatrix4fv(program.getModelView(), 1, false, program.getMVMatrix().getBuffer());
        gl.glUniformMatrix4fv(program.getProjection(), 1, false, program.getPMatrix().getBuffer());
    } 

    protected void render(GL3 gl, int width, int height) {
		

    	gl.glUseProgram(program.getProgram());
    	
        gl.glEnable(GL3.GL_DEPTH_TEST); // Explicitly enable each render...

    	if(eventBuffer.runEvents(gl)) {
    		program.getMVMatrix().identity().multiply(program.getTranslation()).multiply(program.getScale()).multiply(program.getRotation());    		
            gl.glUniformMatrix4fv(program.getModelView(), 1, false, program.getMVMatrix().getBuffer());
    	}

    	gl.glClear(GL3.GL_COLOR_BUFFER_BIT | GL3.GL_STENCIL_BUFFER_BIT | GL3.GL_DEPTH_BUFFER_BIT);
        for(VertexBuffer buffer : transcriptBuffers) {
        	buffer.render(gl);
        }
        
        if(exporter.isEnabled()) {
        	exporter.exportPNG(gl);
        	exporter.disable(gl);
        }
    }
    
    class ImageExporter {
        final FBObject sinkFBO = new FBObject();
        final FBObject multiSampledFBO = new FBObject();
        String path;
        int[] originalRes = new int[2];
        int[] exportRes = new int[2];
        
        public void enable(GL3 gl, int x, int y, String path) {
        	originalRes[0] = width;
        	originalRes[1] = height;
        	this.path = path;

        	sinkFBO.reset(gl, x, y, 0);
        	multiSampledFBO.reset(gl, x, y, MSAA_SAMPLES);
        	multiSampledFBO.bind(gl);

        	reshape(gl, x, y);
        }
        
        public void init(GL3 gl) {
        	if(!sinkFBO.isInitialized()) {
        		System.out.println("sinkFBO init");
        		sinkFBO.init(gl, width,  height,  0);
        		sinkFBO.attachTexture2D(gl, 0, true);
        	}
        	
        	if(!multiSampledFBO.isInitialized()) {
        		System.out.println("multiSampledFBO init");
	        	multiSampledFBO.init(gl, width, height, MSAA_SAMPLES);
	        	if(MSAA_SAMPLES > 0) {
	        		multiSampledFBO.setSamplingSink(sinkFBO);
	        	}
	        	multiSampledFBO.attachColorbuffer(gl, 0, true);
	        	multiSampledFBO.attachRenderbuffer(gl, FBObject.Attachment.Type.DEPTH_STENCIL, 8);
	        	multiSampledFBO.unbind(gl);
        	}
        }
        
        private void exportPNG(GL3 gl) {
            
        	FloatBuffer buffer = FloatBuffer.allocate(width * height * 3);
        	multiSampledFBO.syncSamplingSink(gl);
        	sinkFBO.bind(gl);
            gl.glReadPixels(0, 0, width, height, GL3.GL_RGB, GL3.GL_FLOAT, buffer);
            buffer.rewind();

            int[] rgbArray = new int[width * height];
            for(int y = 0; y < height; ++y) {
                for(int x = 0; x < width; ++x) {
                    int r = (int)(buffer.get() * 255) << 16;
                    int g = (int)(buffer.get() * 255) << 8;
                    int b = (int)(buffer.get() * 255);
                    int i = ((height - 1) - y) * width + x;
                    rgbArray[i] = r + g + b;
                }
            }
     
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            image.setRGB(0, 0, width, height, rgbArray, 0, width);
            File file = new File(path);
            try {
                ImageIO.write(image, "png", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        public boolean isEnabled() {
        	return multiSampledFBO.isBound();
        }
        
        public void disable(GL3 gl) {
        	reshape(gl, originalRes[0], originalRes[1]);
        	multiSampledFBO.unbind(gl);
        	sinkFBO.unbind(gl);
        }
        
    }

    public EventBuffer getEventBuffer() {
    	return eventBuffer;
    }

    
    protected void reshape(GL3 gl, int width, int height) {
    	
    	//get old center
    	Point2D oldC = new Point2D(this.width/2, this.height/2);

    	this.width = width;
    	this.height = height;
        gl.glViewport(0, 0, width, height);
        program.getPMatrix().ortho(0, width, height, 0, -1, 1);
    	
        //get new center
    	Point2D newC = new Point2D(this.width/2, this.height/2);
        program.getTranslation().translateBasic(newC.x - oldC.x, newC.y - oldC.y, 0);
		program.getMVMatrix().identity().multiply(program.getTranslation()).multiply(program.getScale()).multiply(program.getRotation());    		
        gl.glUniformMatrix4fv(program.getModelView(), 1, false, program.getMVMatrix().getBuffer());
		
        guiVB.reshape(gl, width, height);
        
		gl.glUseProgram(program.getProgram());
        gl.glUniformMatrix4fv(program.getProjection(), 1, false, program.getPMatrix().getBuffer());
    		guiVB.setScale(gl, program.getScale().get(Matrix.m00)); // Extract scale from matrix and update the VB

    }

    public void showGUI(boolean show) {
    	guiVB.setEnabled(show);
    	eventBuffer.justGo();
    }
    
    public Matrix getRotation() {
    	return program.getRotation();
    }

        
    /**
     * Converts x and y window coordinates to scene coordinates.
     */
    public Point2D invertCoordinates(float x, float y) {
    	
		Matrix m = new Matrix();
		m.identity().multiply(program.getRotation().transpose()).multiply(program.getScale().scaleInverse()).multiply(program.getTranslation().translateInverse());
		float[] vec = m.multiply(new float[] {x, y, 0, 1});
		return new Point2D(vec[0], vec[1]);
    }
    
    /*
     * Convert from scene coordinates to real coordinates
     */
    public Point2D disinvertCoordinates(float x, float y) {
    	
		Matrix m = new Matrix();
    	m.identity().multiply(program.getTranslation()).multiply(program.getScale()).multiply(program.getRotation());    		
		float[] coord = m.multiply(new float[]{x, y, 0, 1});
		return new Point2D(Math.round(coord[0]), Math.round(coord[1]));
    }
    
    
    /**
     * Converts x and y window coordinates to scene coordinates.
     */
    public Point2D invertTranslation(float x, float y) {
    	
    	Matrix m = new Matrix();
		m.identity().multiply(program.getTranslation().translateInverse());
		float[] vec = m.multiply(new float[] {x,y,0,1});
		return new Point2D(vec[0], vec[1]);
    }


	@Override
	public void apply(GL3 gl, Event e) {
        
        switch(e.type) {
            case PAN:
                applyTranslation(e.x, e.y);
                break;
            case GENERIC:
            		break;
            case ZOOM:
                applyZoom(gl, e.x, e.y, (float) e.modifier);
                break;
            case ROTATE:
                applyRotation(gl, e.x, e.y, (float) e.modifier);
                break;
            case SELECTION:
                applySelection(gl, (ShapeData) e.modifier);
                break;
            case CENTER:
                applyCenter(gl, (Rectangle2D.Double) e.modifier);
            		break;
            case LEGEND:
            		applyLegendScroll(gl, (float) e.modifier);
            		break;
            case RESHAPE:
            	exporter.enable(gl, (int)e.x, (int)e.y, (String) e.modifier);
            	break;
            case IMAGE:
            	imageTileBuffer.setImage(gl, (BufferedImage) e.modifier); 
            	break;
        }	
    	guiVB.setScale(gl, program.getScale().get(Matrix.m00)); // Extract scale from matrix and update the VB

	}
	
	public void applyCenter(GL3 gl, Rectangle2D.Double r) {
		program.getTranslation().identity();
		program.getRotation().identity();
		program.getScale().identity();
		// scene and window coordinates are now 1to1
		// so first let's move the center of the scene to the window center
		program.getTranslation().translateBasic((float) (width/2 - (r.x + r.width/2)), (float) (height/2 - (r.y + r.height/2)), 0);
		
		// now we just figure out how small it has to be to fit in the window size
		float xRatio = (float) (width / r.width);
		float yRatio = (float) (height / r.height);
	    applyZoom(gl, width/2, height/2, (float) Math.min(xRatio, yRatio));
	    guiVB.setRotation(gl, program.getRotation());
	}

	public void applyRotation(GL3 gl, float x, float y, float amount) {
		
		float angle = (float) (amount * Math.PI / 180.0);
						
    	Point2D coord = invertTranslation(x, y);
    	
		Matrix m = new Matrix();
    	m.translate(-coord.x, -coord.y, 0); // move mouse coordinates to origin
    	m.rotateZ(angle);
    	m.translate(coord.x, coord.y, 0);   // move origin back to mouse coordinates

    	program.getRotation().rotateZ(angle);
    	program.getTranslation().translateBasic(-m.get(Matrix.m30), -m.get(Matrix.m31), 0);
    	
    	guiVB.setRotation(gl, program.getRotation());
	}
    
    public void applyTranslation(float x, float y) {
    	program.getTranslation().translateBasic(-x, -y, 0);
    }
    
    public void applyLegendScroll(GL3 gl, float modifier) {
    		guiVB.setLegendScroll(gl, modifier);
    }
	
    public void applyZoom(GL3 gl, float x, float y, float factor) {
    	
    	Point2D coord = invertTranslation(x, y);
    	Matrix m = new Matrix();
    	m.translate(-coord.x, -coord.y, 0); // move mouse coordinates to origin
    	m.scale(factor);						  // scale (from origin)
    	m.translate(coord.x, coord.y, 0);   // move origin back to mouse coordinates

    	program.getScale().scale(factor);
    	program.getTranslation().translateBasic(-m.get(Matrix.m30), -m.get(Matrix.m31), 0);
    	
    	guiVB.setScale(gl, program.getScale().get(Matrix.m00)); // Extract scale from matrix and update the VB
    }
	
    public void applySelection(GL3 gl, ShapeData data) {
    	selection.bufferSelection(gl, data.shape);
    	selection.setComplete(data.complete);
    }
    
    class Invoker implements GLRunnable {

    	Transcript t;
    	
    	public void setTranscript(Transcript t) {
    		this.t = t;
    	}
    	
		@Override
		public boolean run(GLAutoDrawable drawable) {
			guiVB.setSelectedTranscript(t);
			return false;
		}
    	
    }

	public void setSymbolMasterScale(float scale) {
		program.setSymbolMasterScale(scale);
		
	}

	public void setShowAll(boolean selected) {
		program.setShowAll(selected);
	}
}



