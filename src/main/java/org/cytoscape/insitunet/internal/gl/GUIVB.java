package org.cytoscape.insitunet.internal.gl;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.cytoscape.insitunet.internal.Gene;
import org.cytoscape.insitunet.internal.Symbol;
import org.cytoscape.insitunet.internal.typenetwork.Transcript;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.util.GLBuffers;
import com.jogamp.opengl.util.texture.Texture;

/**
 * GUI components are contained within here, that can be toggled on or off. They
 * include the legend, scale, and orientation.
 * 
 * @author John Salamon
 *
 */
public class GUIVB implements VertexBuffer {

	final Integer point_vbo, texcoord_vbo, zero_vbo; // we hijack symbol_vbo for texture coordinates
	final Integer texCoordsEnabledVAO, nonInstancedVAO, symbolVAO;

	final int elementSize = GLBuffers.SIZEOF_FLOAT;
	final int usage = GL3.GL_DYNAMIC_DRAW;
	final int drawType = GL3.GL_LINE_STRIP;
	final int averageWidth;
	final int legendHeight;

	static float COMPASS_RADIUS = 12;

	final int scaleBackgroundOffset, scaleTextOffset, scaleBarOffset, compassOffset, compassArrowOffset,
			selectionThingOffset, legendBackgroundOffset;

	final FloatBuffer buffer, texCoordBuffer, zeroBuffer;
	final Texture zeroTexture = TextRenderer.zeroTexture();

	final ProgramData program;

	final Matrix identity = new Matrix();
	final Matrix rotation = new Matrix();

	int width, height;

	float scale = 1;
	float scroll = 0;
	final List<Gene> genes;
	final TextRenderer textRenderer;
	List<Texture> textures = new ArrayList<>();
	Texture scaleText;
	Transcript selectedTranscript = null;

	final float[] selectionThing = new float[] { -1f, -0.6f, -1f, -1f, -1f, -1f, -0.6f, -1f, 1f, 0.6f, 1f, 1f, 1f, 1f,
			0.6f, 1f, -1f, 0.6f, -1f, 1f, -1f, 1f, -0.6f, 1f, 1f, -0.6f, 1f, -1f, 1f, -1f, 0.6f, -1f, };

	private boolean enabled = true;

	public void setSelectedTranscript(Transcript t) {
		selectedTranscript = t;
	}

	public void setEnabled(boolean b) {
		enabled = b;
	}

	public GUIVB(GL3 gl, ProgramData program, List<Gene> genes) {
		// Most geometry can be stored in a single buffer, so let's calculate how big it
		// needs to be
		int bufsize = genes.size() * 6 * 2 // For legend label quads
				+ 6 * 2 // For scalebar text
				+ genes.size() * 6 * 2 // for legend background
				+ 6 * 2 // For scalebar background quad
				+ 4 * 2 // For scalebar
				+ 12 * 2 // For compass
				+ 6 * 2// Compass arrow
				+ 16 * 2; // the selection thingy
		// Save the start position of the unique objects to use later
		scaleTextOffset = genes.size() * 6 * 2;
		legendBackgroundOffset = scaleTextOffset + 6 * 2;
		scaleBackgroundOffset = legendBackgroundOffset + genes.size() * 6 * 2;
		scaleBarOffset = scaleBackgroundOffset + 6 * 2;
		compassOffset = scaleBarOffset + 4 * 2;
		compassArrowOffset = compassOffset + 12 * 2;
		selectionThingOffset = compassArrowOffset + 6 * 2;

		this.buffer = FloatBuffer.allocate(bufsize);
		// We fill an equivalent length buffer with zeros because I'm too lazy to use
		// multiple shaders
		this.zeroBuffer = FloatBuffer.allocate(bufsize);
		// we also need a buffer for texture coordinates
		this.texCoordBuffer = FloatBuffer.allocate(genes.size() * 6 * 2 + 6 * 2); // it only needs to be the size of the
																					// label section of the buffer + 1
																					// more for scaletext
		this.program = program;
		this.genes = genes;

		// initialize VBOs
		int vbo[] = new int[3];
		gl.glGenBuffers(3, vbo, 0);
		this.point_vbo = vbo[0];
		this.texcoord_vbo = vbo[1];
		this.zero_vbo = vbo[2];
		

		// initialize text rendering
		textRenderer = new TextRenderer(gl, "Anonymous Pro", 13);

		// Generate legend name text textures / texcoords
		float[] texcoordQuad = VertexBuffer.makeQuad(0, 0, 1, 1);
		int i = 0;
		int averageWidth = 0;
		for (; i < genes.size(); i++) {
			Texture t = textRenderer.stringToTexture(genes.get(i).getName());
			textures.add(t);
			averageWidth += t.getImageWidth();
			float[] labelQuad = VertexBuffer.makeQuad(0, 0, t.getImageWidth(), t.getHeight());
			int position = labelQuad.length * i;
			for (int j = 0; j < labelQuad.length; j++) {
				buffer.put(position + j, labelQuad[j]);
				texCoordBuffer.put(position + j, texcoordQuad[j]);
			}
		}
		this.legendHeight = textures.get(0).getImageHeight() * textures.size();

		averageWidth /= genes.size();
		averageWidth += 15;
		this.averageWidth = averageWidth;
		int position = texcoordQuad.length * i;
		for (int j = 0; j < texcoordQuad.length; j++) { // do one more for the other scale label
			texCoordBuffer.put(position + j, texcoordQuad[j]);
		}

		for (i = 0; i < genes.size(); i++) {
			Texture t = textures.get(i);
			float[] labelQuad = VertexBuffer.makeQuad(0, 0, t.getImageWidth() + 15, t.getHeight());
			position = labelQuad.length * i;
			for (int j = 0; j < labelQuad.length; j++) {
				buffer.put(legendBackgroundOffset + position + j, labelQuad[j]);
			}
		}

		updateSelectionThing(gl);
		updateScaleBar(gl);
		updateCompass(gl);
		bufferData(gl);

		// Initialize VAOs
		IntBuffer vao = GLBuffers.newDirectIntBuffer(3);
		gl.glGenVertexArrays(3, vao);
		nonInstancedVAO = vao.get(0);
		texCoordsEnabledVAO = vao.get(1);
		symbolVAO = vao.get(2);

		genVertexArray(gl);
	}

	void updateSelectionThing(GL3 gl) {
		for (int i = 0; i < selectionThing.length; i++) {
			buffer.put(selectionThingOffset + i, (float) (Math.round(selectionThing[i] * 7.0) + 0.5));
		}
	}

	/**
	 * Update the scale for the GUI display
	 */
	public void setScale(GL3 gl, float scale) {
		this.scale = scale;
		updateScaleBar(gl);
	}
	
	public void adjustScroll() {
		if (this.scroll > 0) {
			this.scroll = 0;
		} else if (this.legendHeight > this.height && Math.abs(this.scroll) > this.legendHeight - this.height) {
			this.scroll = -1 * (this.legendHeight - this.height);
		}
		
	}
	
	public void setLegendScroll(GL3 gl, float amount) {
		if(this.legendHeight < this.height) return;
		this.scroll += (amount * 8);
		adjustScroll();
	}

	/**
	 * Update the rotation for the GUI display
	 */
	public void setRotation(GL3 gl, Matrix r) { // we actually want the whole rotation matrix here
		rotation.copy(r); // copy rotation matrix into local
	}

	/**
	 * Recalculate the width of the scale bar.
	 */
	public void updateScaleBar(GL3 gl) {

		double exponent = Math.ceil(Math.log10(scale));
		double divisor = Math.pow(10, exponent);
		float barwidth = (float) (100.0 / divisor);
		scaleText = textRenderer.stringToTexture(String.format("%4.0epx", barwidth));

		Point2D origin = new Point2D(width - 20, height - 15);
		float farpoint = origin.x - barwidth * scale;
		float[] scalebar = { origin.x, origin.y + 5, origin.x, origin.y, farpoint, origin.y, farpoint, origin.y + 5, };
		for (int i = 0; i < scalebar.length; i++) {
			buffer.put(scaleBarOffset + i, Math.round(scalebar[i]) + 0.5f);
		}

		float[] scaleBackground = VertexBuffer.makeQuad(farpoint - 5, origin.y - 5, origin.x + 5, origin.y + 10);
		for (int i = 0; i < scaleBackground.length; i++) {
			buffer.put(scaleBackgroundOffset + i, scaleBackground[i]);
		}

		origin = new Point2D(width - 20 - scaleText.getWidth(), height - 20 - scaleText.getHeight());
		float[] scaleTextGeometry = VertexBuffer.makeQuad(origin.x, origin.y, origin.x + scaleText.getWidth(),
				origin.y + scaleText.getHeight());
		for (int i = 0; i < scaleTextGeometry.length; i++) {
			buffer.put(scaleTextOffset + i, scaleTextGeometry[i]);
		}

		bufferData(gl);
	}

	void updateCompass(GL3 gl) {
		float[] circle = VertexBuffer.makeCircle(24, COMPASS_RADIUS);
		for (int i = 0; i < circle.length; i++) {
			buffer.put(compassOffset + i, circle[i]);
		}
		float[] arrow = new float[] { -COMPASS_RADIUS / 3, 0, 0, -COMPASS_RADIUS, 0, -COMPASS_RADIUS / 6,
				COMPASS_RADIUS / 3, 0, 0, -COMPASS_RADIUS, 0, -COMPASS_RADIUS / 6 };
		for (int i = 0; i < arrow.length; i++) {
			buffer.put(compassArrowOffset + i, arrow[i]);
		}
	}

	public void reshape(GL3 gl, int width, int height) {
		this.width = width;
		this.height = height;
		adjustScroll();

		updateScaleBar(gl);
	}

	public void bufferData(GL3 gl) {
		// The GLSL 3.3 spec states that uninitialized attributes are undefined,
		// So I'm uploading zeroes to symbol_geometry when instancing isn't in use.
		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, zero_vbo);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, zeroBuffer.capacity() * elementSize, zeroBuffer, GL3.GL_STATIC_DRAW);

		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, point_vbo);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, buffer.capacity() * elementSize, buffer, usage);

		gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, texcoord_vbo);
		gl.glBufferData(GL3.GL_ARRAY_BUFFER, texCoordBuffer.capacity() * elementSize, texCoordBuffer,
				GL3.GL_STATIC_DRAW);
	}

	public void genVertexArray(GL3 gl) {

		int stride = 0;
		int offset = 0;

		gl.glBindVertexArray(nonInstancedVAO);
		{
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, zero_vbo);
			{
				gl.glEnableVertexAttribArray(program.getSymbol());
				gl.glVertexAttribPointer(program.getSymbol(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);

			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, point_vbo);
			{
				gl.glEnableVertexAttribArray(program.getPoint());
				gl.glVertexAttribPointer(program.getPoint(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
		}
		gl.glBindVertexArray(0);

		gl.glBindVertexArray(texCoordsEnabledVAO);
		{
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, texcoord_vbo);
			{
				gl.glEnableVertexAttribArray(program.getSymbol());
				gl.glVertexAttribPointer(program.getSymbol(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);

			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, point_vbo);
			{
				gl.glEnableVertexAttribArray(program.getPoint());
				gl.glVertexAttribPointer(program.getPoint(), 2, GL.GL_FLOAT, false, stride, offset);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
		}
		gl.glBindVertexArray(0);

		gl.glBindVertexArray(symbolVAO);
		{
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, program.getSymbolBuffer());
			{
				gl.glEnableVertexAttribArray(program.getSymbol());
				gl.glVertexAttribPointer(program.getSymbol(), 2, GL.GL_FLOAT, false, 0, 0);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);

			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, zero_vbo);
			{
				gl.glEnableVertexAttribArray(program.getPoint());
				gl.glVertexAttribPointer(program.getPoint(), 2, GL.GL_FLOAT, false, 0, 0);
				gl.glVertexAttribDivisor(1, 1);
			}
			gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
		}
		gl.glBindVertexArray(0);

	}

	public void drawScaleBar(GL3 gl) {
		gl.glUniformMatrix4fv(program.getModelView(), 1, false, identity.getBuffer()); // Set modelview to identity
																						// matrix
		gl.glUniform1f(program.getPointScale(), 0); // set symbol scale uniform to zero

		gl.glBindVertexArray(texCoordsEnabledVAO); // Texture coordinates bound to vbo
		{
			gl.glUniform4f(program.getColour(), 0.0f, 0.0f, 0.0f, 0.8f); // Set colour to black, mostly opaque
			gl.glDrawArrays(GL3.GL_TRIANGLES, scaleBackgroundOffset / 2, 6); // draw scalebar background quad
			gl.glDrawArrays(GL3.GL_TRIANGLES, scaleTextOffset / 2, 6); // draw scalebar text background quad

			scaleText.bind(gl); // bind the scale text texture
			gl.glUniform4f(program.getColour(), 1, 1, 1, 0); // Set transparent, white. The texture and colour are
																// added, so opacity will only be on the text.
			gl.glDrawArrays(GL3.GL_TRIANGLES, scaleTextOffset / 2, 6); // draw the text
		}
		gl.glBindVertexArray(0);

		gl.glBindVertexArray(nonInstancedVAO); // Zero VBO bound to symbol_geometry
		{
			gl.glUniform4f(program.getColour(), 1.0f, 1.0f, 1.0f, 1.0f); // Set colour to white
			gl.glDrawArrays(GL3.GL_LINE_STRIP, scaleBarOffset / 2, 4); // draw the actual scale bar
		}
		gl.glBindVertexArray(0);

		gl.glUniformMatrix4fv(program.getModelView(), 1, false, program.getMVMatrix().getBuffer()); // reset the
																									// modelview matrix
	}

	/**
	 * Draws the legend, partially transparent black backgrounds, labels with
	 * colours corresponding to genes. Should also draw symbols here at some point
	 */
	
	// TODO:
	// 1. Identify point at which scrolling should scroll legend - take average of length
	// 2. Simply use the eventbuffer to make a smooth scroll, pass it in here
	// 3. ???
	// 4. Profit
	public void drawLegend(GL3 gl) {
		gl.glUniformMatrix4fv(program.getModelView(), 1, false, identity.getBuffer());

		gl.glBindVertexArray(texCoordsEnabledVAO); // texture coordinates are now bound
		{
			Matrix textMatrix = new Matrix(); // A matrix for legend label offsets
			textMatrix.translateBasic(0, this.scroll, 0); // translate the text matrix down


			gl.glUniform4f(program.getColour(), 0, 0, 0, 0.8f); // Mostly opaque black background colour
			for (int i = 0; i < genes.size(); i++) {
				Texture t = textures.get(i); // bind each text texture in turn
				gl.glUniformMatrix4fv(program.getModelView(), 1, false, textMatrix.getBuffer()); // bind the text matrix
				gl.glDrawArrays(GL3.GL_TRIANGLES, legendBackgroundOffset / 2 + i * 6, 6); // draw each text thing
				textMatrix.translateBasic(0, t.getHeight(), 0); // translate the text matrix down
			}
		}
		gl.glBindVertexArray(0);

		gl.glUniform1f(program.getPointScale(), 4f);

		// For the legend we'll draw the symbols, then the names
		// We'll omit scaling the symbols for the sake of making everything fit
		gl.glBindVertexArray(symbolVAO);
		{
			// TODO: This won't actually work, just to it properly with instancing
			// Or, I suppose you could scale and translate each time but who can be
			// bothered?
			Matrix matrix = new Matrix().translateBasic(5.5f, 6.5f + this.scroll, 0);
			int i = 0;
			for (Gene gene : genes) {
				float[] c = gene.getColorRGB();
				gl.glUniform4f(program.getColour(), c[0], c[1], c[2], 1);
				gl.glUniformMatrix4fv(program.getModelView(), 1, false, matrix.getBuffer());

				/*
				 * NOTE: From my tests at least, when using 1e6+ points, GL_TRIANGLES performs
				 * significantly better than GL_LINE_LOOP.
				 */
				Symbol s = gene.getSymbol();
				gl.glDrawArraysInstanced(s.getEnum(), gene.getSymbol().getOffset() / 2, s.getLength() / 2, 1);

				matrix.translateBasic(0, textures.get(i).getHeight(), 0);
				i++;
			}

		}
		gl.glBindVertexArray(0);

		gl.glUniform1f(program.getPointScale(), 0); // set symbol scale uniform to zero

		gl.glBindVertexArray(texCoordsEnabledVAO); // texture coordinates are now bound
		{

			gl.glUniform4f(program.getColour(), 0, 0, 0, 0.0f); // Mostly opaque black background colour
			Matrix textMatrix = new Matrix().translateBasic(15, 0 + this.scroll, 0);
			for (int i = 0; i < textures.size(); i++) {
				Texture t = textures.get(i);
				Gene g = genes.get(i);
				float[] c = g.getColorRGB();
				gl.glUniform4f(program.getColour(), c[0], c[1], c[2], 0); // bind the gene colour

				// gl.glUniform4f(program.getColour(), c[0], c[1], c[2], 0); // bind the gene
				// colour
				gl.glUniformMatrix4fv(program.getModelView(), 1, false, textMatrix.getBuffer()); // bind the text matrix
				t.bind(gl); // bind the text texture
				gl.glDrawArrays(GL3.GL_TRIANGLES, i * 6, 6); // draw the legend text
				textMatrix.translateBasic(0, t.getHeight(), 0); // shift text matrix down
			}
			zeroTexture.bind(gl); // done, bind the zero texture
		}
		gl.glBindVertexArray(0);

		gl.glUniformMatrix4fv(program.getModelView(), 1, false, program.getMVMatrix().getBuffer());
	}

	public void drawCompass(GL3 gl) {

		Matrix compassMatrix = new Matrix();
		compassMatrix.translateBasic(width - COMPASS_RADIUS - 10, COMPASS_RADIUS + 5, 0);

		gl.glUniformMatrix4fv(program.getModelView(), 1, false, compassMatrix.getBuffer());
		gl.glUniform1f(program.getPointScale(), 0); // set symbol scale uniform to zero

		gl.glBindVertexArray(nonInstancedVAO); // Zero VBO bound to symbol_geometry
		{
			gl.glUniform4f(program.getColour(), 0, 0, 0, 0.8f); // background colour
			gl.glDrawArrays(GL3.GL_TRIANGLE_FAN, compassOffset / 2, 12); // fill it in
			gl.glUniform4f(program.getColour(), 1.0f, 1.0f, 1.0f, 1.0f); // Set colour to white
			gl.glDrawArrays(GL3.GL_LINE_LOOP, compassOffset / 2, 12); // draw the circle

			compassMatrix.multiply(rotation);
			gl.glUniformMatrix4fv(program.getModelView(), 1, false, compassMatrix.getBuffer());
			gl.glUniform4f(program.getColour(), 1, 0, 0, 0.8f); // Set colour to red
			gl.glDrawArrays(GL3.GL_TRIANGLES, compassArrowOffset / 2, 6); // draw the arrow
		}
		gl.glBindVertexArray(0);

		gl.glUniformMatrix4fv(program.getModelView(), 1, false, program.getMVMatrix().getBuffer()); // reset the
																									// modelview matrix

	}

	public void drawSelectedTranscript(GL3 gl) {
		int index = selectedTranscript.getGene().getIndex();
		int bufferOffset = 6 * index;
		Texture texture = textures.get(index);

		gl.glUniformMatrix4fv(program.getModelView(), 1, false, identity.getBuffer());
		gl.glUniform1f(program.getPointScale(), 0); // set symbol scale uniform to zero

		Matrix textMatrix = new Matrix(); // A matrix for legend label offsets
		Point2D p = selectedTranscript.getPoint();
		float[] vec = program.getMVMatrix().multiply(new float[] { p.x, p.y, 0, 1 });

		gl.glBindVertexArray(texCoordsEnabledVAO); // texture coordinates are now bound
		{

			vec[0] = Math.round(vec[0]);
			vec[1] = Math.round(vec[1]); // so it's pixel perfect
			gl.glUniform4f(program.getColour(), 0, 0, 0, 0.8f); // Mostly opaque black background colour
			textMatrix.translateBasic(vec[0] + 15, vec[1] - texture.getHeight() / 2, 0); // translate the text matrix
																							// down
			gl.glUniformMatrix4fv(program.getModelView(), 1, false, textMatrix.getBuffer()); // bind the text matrix
			gl.glDrawArrays(GL3.GL_TRIANGLES, bufferOffset, 6); // draw each text thing

			float[] c = selectedTranscript.getGene().getColorRGB();
			gl.glUniform4f(program.getColour(), c[0], c[1], c[2], 0); // bind the gene colour

			texture.bind(gl); // bind the text texture
			gl.glDrawArrays(GL3.GL_TRIANGLES, bufferOffset, 6); // draw the legend text
			zeroTexture.bind(gl); // done, bind the zero texture
		}

		gl.glBindVertexArray(nonInstancedVAO); // Zero VBO bound to symbol_geometry
		{
			textMatrix.identity().translateBasic(vec[0], vec[1], 0); // translate the text matrix down
			gl.glUniformMatrix4fv(program.getModelView(), 1, false, textMatrix.getBuffer()); // bind the text matrix
			gl.glLineWidth(4.0f);
			gl.glUniform4f(program.getColour(), 0.0f, 0.0f, 0.0f, 1.0f); // Set colour to black
			gl.glDrawArrays(GL3.GL_LINES, selectionThingOffset / 2, selectionThing.length / 2); // draw the actual scale
																								// bar
			gl.glLineWidth(1.0f);
			gl.glUniform4f(program.getColour(), 1.0f, 1.0f, 1.0f, 1.0f); // Set colour to white
			gl.glDrawArrays(GL3.GL_LINES, selectionThingOffset / 2, selectionThing.length / 2); // draw the actual scale
																								// bar

		}
		gl.glBindVertexArray(0);

		gl.glBindVertexArray(0);
		gl.glUniformMatrix4fv(program.getModelView(), 1, false, program.getMVMatrix().getBuffer());

	}

	public void render(GL3 gl) {

		if (enabled) {
			gl.glUseProgram(program.getProgram());

			gl.glActiveTexture(GL3.GL_TEXTURE0);
			gl.glUniform1i(program.getSampler(), 0);
			gl.glDisable(GL3.GL_DEPTH_TEST);
			if (selectedTranscript != null) {
				drawSelectedTranscript(gl);
			}
			drawLegend(gl);
			drawScaleBar(gl);
			drawCompass(gl);
			gl.glEnable(GL3.GL_DEPTH_TEST);

			gl.glUseProgram(0);
		}
	}
}
