package org.cytoscape.insitunet.internal.gl;

import java.util.List;

import org.cytoscape.insitunet.internal.Symbol;
import org.cytoscape.insitunet.internal.Symbol.SymbolList;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.util.glsl.ShaderCode;
import com.jogamp.opengl.util.glsl.ShaderProgram;

/**
 * Used to create programs and store the various OpenGL objects required.
 * 
 * @author jrs
 *
 */
public class ProgramData {

	Integer programLocation, // Program itself
			projectionLocation, modelViewLocation, colourLocation, pointScaleLocation, sampler, // Uniforms
			pointLocation, symbolLocation; // Attributes

	final Matrix projection = new Matrix();
	final Matrix modelView = new Matrix();

	final Matrix translation = new Matrix();
	final Matrix scale = new Matrix();
	final Matrix rotation = new Matrix();

	final List<Symbol> symbols = SymbolList.genSymbolList();
	Integer symbolBuffer;

	private float masterScale = 4f;

	private boolean showAll = true;

	ProgramData() {

	}
	
	public void init(GL3 gl, String name) {
		final ShaderCode vp = ShaderCode.create(gl, GL3.GL_VERTEX_SHADER, GLPanel.class, "shader", null, name, "vert",
				null, false);
		final ShaderCode fp = ShaderCode.create(gl, GL3.GL_FRAGMENT_SHADER, GLPanel.class, "shader", null, name, "frag",
				null, false);
		ShaderProgram p = new ShaderProgram();
		p.add(gl, vp, System.err);
		p.add(gl, fp, System.err);
		p.init(gl);
		programLocation = p.program();
		p.link(gl, System.err);

		projectionLocation = gl.glGetUniformLocation(programLocation, "projection");
		modelViewLocation = gl.glGetUniformLocation(programLocation, "modelview");
		colourLocation = gl.glGetUniformLocation(programLocation, "colour");
		pointScaleLocation = gl.glGetUniformLocation(programLocation, "symbol_scale");
		sampler = gl.glGetUniformLocation(programLocation, "sampler");

		pointLocation = gl.glGetAttribLocation(programLocation, "point_location");
		symbolLocation = gl.glGetAttribLocation(programLocation, "symbol_geometry");

		gl.glUseProgram(getProgram());

		symbolBuffer = SymbolList.makeBuffer(gl, symbols);
		if (modelViewLocation == -1) {
			System.err.println("uniform 'modelview' not found!");
		}
	}

	public List<Symbol> getSymbolList() {
		return symbols;
	}

	public int getNumSymbols() {
		return symbols.size();
	}

	public Integer getSymbolBuffer() {
		return symbolBuffer;
	}

	public Matrix getPMatrix() {
		return projection;
	}

	public Matrix getTranslation() {
		return translation;
	}

	public Matrix getScale() {
		return scale;
	}

	public Matrix getRotation() {
		return rotation;
	}

	public Matrix getMVMatrix() {
		return modelView;
	}

	public Integer getProgram() {
		return programLocation;
	}

	public Integer getColour() {
		return colourLocation;
	}

	public Integer getPoint() {
		return pointLocation;
	}

	public Integer getPointScale() {
		return pointScaleLocation;
	}

	public Integer getSampler() {
		return sampler;
	}

	public Integer getSymbol() {
		return symbolLocation;
	}

	public Integer getModelView() {
		return modelViewLocation;
	}

	public Integer getProjection() {
		return projectionLocation;
	}

	public void setSymbolMasterScale(float masterScale) {
		this.masterScale = masterScale;
	}

	public float getSymbolMasterScale() {
		return masterScale;
	}

	public void setShowAll(boolean selected) {
		showAll = selected;
	}

	public boolean getShowAll() {
		return showAll;
	}

}