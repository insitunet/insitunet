package org.cytoscape.insitunet.internal;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableColumn;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.cytoscape.insitunet.internal.typenetwork.Transcript;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import edu.wlu.cs.levy.CG.KDTree;

public class InsituNetImporter extends AbstractTask {

	InsituNetActivator activator;

	final JFileChooser fc = new JFileChooser();
	final JFrame frame;
	final List<String> headers = new ArrayList<String>();
	final JTextField fileField = new JTextField();
	final JTable table;
	final JPanel messagePanel = new JPanel();
	String filename;
	CSVParser parser;
	private int errorsOccured;

	@SuppressWarnings("serial")
	public InsituNetImporter(InsituNetActivator activator) {
		this.activator = activator;
		frame = activator.getCSAA().getCySwingApplication().getJFrame();
		FileFilter filter = new FileNameExtensionFilter("csv files", "csv");
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);

		messagePanel.setLayout(new GridBagLayout());
		JButton button = new JButton("Browse");
		fileField.setEditable(false);
		messagePanel.add(fileField, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.LINE_START,
				GridBagConstraints.HORIZONTAL, new Insets(4, 4, 4, 0), 1, 1));
		messagePanel.add(button, new GridBagConstraints(1, 0, 1, 1, 0.1, 0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(4, 0, 4, 4), 1, 1));

		String[] columns = { "Field", "Input column" };
		Object[][] data = { { "Name", "" }, { "X coordinate", "" }, { "Y coordinate", "" } };
		this.table = new JTable(data, columns) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return column == 1 ? true : false;
			}
		};

		JPanel tablePanel = new JPanel();
		tablePanel.setLayout(new BorderLayout());

		tablePanel.add(new JLabel("Please ensure that the columns are correctly matched."), BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		Dimension d = new Dimension(table.getPreferredScrollableViewportSize().width, table.getPreferredSize().height);
		table.setPreferredScrollableViewportSize(d);
		tablePanel.add(scrollPane, BorderLayout.CENTER);
		messagePanel.add(tablePanel, new GridBagConstraints(0, 1, 2, 1, 0.1, 0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(4, 0, 4, 4), 1, 1));

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!getFile())
					return;
				try {
					parseFile();
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(frame, "File parsing error!", "Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
		});
	}

	public boolean getFile() {
		int returnVal = fc.showOpenDialog(frame);
		if (!(returnVal == JFileChooser.APPROVE_OPTION))
			return false;
		String filename = fc.getSelectedFile().getName();
		if (activator.doesImportExist(filename)) {
			JOptionPane.showMessageDialog(frame, "A file named " + filename + " has already been imported!", "Error",
					JOptionPane.WARNING_MESSAGE);
			return false;
		}
		this.filename = filename;
		return true;
	}

	public boolean parseFile() throws IOException {
		File file = new File(fc.getSelectedFile().getAbsolutePath());
		FileReader in = new FileReader(file);
		parser = CSVFormat.EXCEL.parse(in);
		// First parse the headers
		List<String> headers = new ArrayList<String>();
		for (CSVRecord record : parser) {
			for (int i = 0; i < record.size(); i++) {
				headers.add(record.get(i));
			}
			break;
		}
		if (headers.size() < 3) {
			JOptionPane.showMessageDialog(frame, "Too few columns (at least 3 required)", "Error",
					JOptionPane.WARNING_MESSAGE);
			in.close();
			return false;
		}
		fileField.setText(fc.getSelectedFile().getAbsolutePath());
		this.headers.clear();
		this.headers.addAll(headers);

		this.table.getModel().setValueAt(headers.get(0), 0, 1);
		this.table.getModel().setValueAt(headers.get(1), 1, 1);
		this.table.getModel().setValueAt(headers.get(2), 2, 1);

		JComboBox<String> options = new JComboBox<>();
		for (String s : headers) {
			options.addItem(s);
		}

		TableColumn column = table.getColumnModel().getColumn(1);
		column.setCellEditor(new DefaultCellEditor(options));
		return true;
	}

	public JPanel getMessagePanel() {
		return messagePanel;
	}

	@Override
	public void run(TaskMonitor monitor) throws Exception {

		monitor.setTitle("Importing data");
		monitor.setStatusMessage("Parsing csv file...");

		final List<Gene> genes = new ArrayList<Gene>();

		int name_column = headers.indexOf(table.getModel().getValueAt(0, 1));
		int x_column = headers.indexOf(table.getModel().getValueAt(1, 1));
		int y_column = headers.indexOf(table.getModel().getValueAt(2, 1));

		int index = 0;
		for (CSVRecord record : parser) {

			if (cancelled)
				return;

			if (record.size() != headers.size())
				continue; // record must be the same size as header

			String name = record.get(name_column);

			// Discard unknown reads
			if (name.equals("NNNN"))
				continue;

			Gene g = new Gene(name, genes.size());
			if (!genes.contains(g)) {
				genes.add(g);
			} else {
				g = genes.get(genes.indexOf(g));
			}

			double x, y;
			try {
				x = Double.parseDouble(record.get(x_column));
				y = Double.parseDouble(record.get(y_column));
			} catch (NumberFormatException e) {
				errorsOccured++;
				continue;
			}
			Transcript t = new Transcript(x, y, g, index++);
			g.addTranscript(t);
		}
		parser.close();

		monitor.setStatusMessage("Constructing kdTree...");

		KDTree<Transcript> tree = new KDTree<Transcript>(2);
		int i = 0;
		for (Gene gene : genes) {
			for (Transcript t : gene.getTranscripts()) {
				if (cancelled)
					return;
				monitor.setProgress((double) i++ / index);
				try {
					tree.insert(new double[] { t.getX(), t.getY() }, t);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		if (errorsOccured > 0) {
			monitor.showMessage(TaskMonitor.Level.WARN, errorsOccured + " records could not be parsed correctly");
		}

		/**
		 * Sort genes in alphabetical order
		 */
		Collections.sort(genes, new Comparator<Gene>() {
			@Override
			public int compare(Gene g1, Gene g2) {
				return g1.getName().compareToIgnoreCase(g2.getName());
			}
		});

		/**
		 * Update the internal index.
		 */
		for (int j = 0; j < genes.size(); j++) {
			genes.get(j).setIndex(j);
		}

		// Package all information into an InsituDataset
		InsituDataset dataset = new InsituDataset(activator, filename, genes, tree, index);
		activator.finalizeImport(dataset);
	}

}
