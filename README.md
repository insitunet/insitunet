# THIS REPOSITORY HAS BEEN MOVED TO [https://bitbucket.org/lynnlab/insitunet](https://bitbucket.org/lynnlab/insitunet) #


A [Cytoscape](http://cytoscape.org) (3.2+) app for visualisation and analysis of spatially-resolved gene expression data.

### Documentation ##

[Documentation (pdf)](https://bytebucket.org/insitunet/insitunet/wiki/InsituNet_Documentation.pdf)

[Documentation (html)](https://sala6.bitbucket.io/)

### Quick summary ###

* Import spatial gene expression data as csv files (name,x,y)
* Find transcripts that are spatially co-expressed within different regions
* Synchronise multiple networks and see the differences
* Apply various filtering options
* [Find other information on the InsituNet Wiki](https://bitbucket.org/insitunet/insitunet/wiki/Home)

### Compiling from source ###

InsituNet uses maven for building. It should be possible to build the jar with `mvn install`.
